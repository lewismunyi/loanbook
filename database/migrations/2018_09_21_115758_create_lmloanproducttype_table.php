<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLmloanproducttypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lmloanproducttype', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aggregateLoans');
            $table->integer('loanType');
            $table->integer('productCategory');
            $table->string('productCode');
            $table->string('productDescription');
            $table->string('loanProductSource');
            $table->integer('maximumAppealThreshhold');
            $table->integer('allowsAppeal');
            $table->integer('billImmediately');
            $table->string('requiredCourseOpeningDates');
            $table->integer('requiresOfficeLetter');
            $table->integer('letterExpiryPeriod');
            $table->bigInteger('dimensionsDefault');
            $table->bigInteger('dimensionsDefaultRecovery');
            $table->string('modifiedDateTime');
            $table->string('modifiedBy');
            $table->string('createdDateTime');
            $table->string('createdBy');
            $table->string('dataAreAid');
            $table->bigInteger('recVersion');
            $table->bigInteger('partition');
            $table->bigInteger('recId');
            $table->integer('requiredEmployerApproval');
            $table->string('hasFinancialLiteracy');
            $table->string('productID');
            $table->string('county');
            $table->string('constituency');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lmloanproducttype');
    }
}
