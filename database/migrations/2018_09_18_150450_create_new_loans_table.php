<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_loans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('id_no');
            $table->string('reg_no');
            $table->integer('serial_no');
            $table->integer('academic_year');
            $table->integer('principle_loan');
            $table->string('institution')->nullable();
            $table->string('year_of_completion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_loans');
    }
}
