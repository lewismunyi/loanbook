<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanStatementDetailedHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_statement_detailed_headers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('names');
            $table->string('idnumber');
            $table->string('loanproductcode');
            $table->string('PRODUCTDESCRIPTION');
            $table->string('loanserialno');
            $table->string('accountnum');
            $table->float('loanprincipal')->default(0);
            $table->float('outstanding_interest')->default(0);
            $table->float('outstanding_principal')->default(0);
            $table->float('outstanding_insurance')->default(0);
            $table->float('outstanding_ledgerfee')->default(0);
            $table->float('outstanding_penalty')->default(0);
            $table->float('running_balance')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_statement_detailed_headers');
    }
}