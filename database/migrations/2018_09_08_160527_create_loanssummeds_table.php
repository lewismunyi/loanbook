<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanssummedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loanssummeds', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('rec_id');
            $table->string('username')->nullable();
            $table->string('phone_num')->nullable();
            $table->string('email')->nullable();
            $table->string('id_no')->nullable();
            $table->string('account_num')->nullable();
            $table->string('university')->nullable();
            $table->string('institution_code')->nullable();
            $table->string('student_reg_num')->nullable();
            $table->string('start_year')->nullable();
            $table->string('end_year')->nullable();
            $table->string('academic_year')->nullable();
            $table->string('loan_serial_num')->nullable();
            $table->string('loan_category')->nullable();
            $table->string('loan_category_code')->nullable();
            $table->double('principal_disbursed')->nullable();
            $table->double('outstanding_balance')->nullable();
            $table->integer('is_matured')->nullable();
            $table->date('loan_disbursement_date')->nullable();
            $table->date('loan_maturity_date')->nullable();
            $table->string('loan_status')->nullable();
           // $table->enum('loan_state', ['Performing', 'Default' ,'Dormant','Cleared']); 
            $table->integer('statements_migration_status')->nullable();
            $table->date('statements_migration_date')->nullable();     
            $table->date('completed_at')->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loanssummeds');
    }
}
