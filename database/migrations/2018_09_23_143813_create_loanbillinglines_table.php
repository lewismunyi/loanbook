<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanbillinglinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loanbillinglines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('loanbillingheaders_id');
            $table->string('accountnum');
            $table->string('idno');
            $table->date('lastpaydate');
            $table->string('loanproductcode');
            $table->string('loanserialno');
            $table->float('outstanding_balance');
            $table->float('repaymentrate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loanbillinglines');
    }
}
