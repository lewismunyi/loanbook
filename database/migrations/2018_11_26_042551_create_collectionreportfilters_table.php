<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionreportfiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collectionreportheaders', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('minrecid');
            $table->bigInteger('maxrecid');
            $table->bigInteger('currentrecid');
            $table->string('filters');
            $table->integer('totalcount');
            $table->float('totalamount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collectionreportfilters');
    }
}
