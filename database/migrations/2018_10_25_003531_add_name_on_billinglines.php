<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameOnBillinglines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loanbillinglines', function (Blueprint $table) {
            //
             $table->string('name');            
             $table->string('phone')->nullable();
             $table->string('email')->nullable();;
               
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loanbillinglines', function (Blueprint $table) {
            //
        });
    }
}
