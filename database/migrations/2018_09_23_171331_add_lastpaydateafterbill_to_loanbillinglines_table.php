<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastpaydateafterbillToLoanbillinglinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loanbillinglines', function (Blueprint $table) {
            //
               $table->date('lastpaydateafterbill')->nullable();
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loanbillinglines', function (Blueprint $table) {
            //
        });
    }
}
