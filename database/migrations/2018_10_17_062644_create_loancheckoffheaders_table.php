<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoancheckoffheadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('loancheckoffheaders', function (Blueprint $table) {
                 $table->increments('id');
                 $table->bigInteger('recid');
                 $table->string('checkoffbatchno');
                 $table->float('amount');            
                 $table->date('checkoffdate');
                 $table->string('checkoffperiod');            
                 $table->string('custpaymreference');
                 $table->date('dateentered');            
                 $table->date('documentdate');
                 $table->string('documentno');            
                 $table->string('employercode');
                 $table->tinyInteger('posted');            
                 $table->string('postedby');
                 $table->date('dateposted');            
                 $table->string('paymmode');
                 $table->string('postedjournalnum');            
                 $table->string('penaltyamount');
                 $table->string('reversed');          
                 $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loancheckoffheaders');
    }
}
