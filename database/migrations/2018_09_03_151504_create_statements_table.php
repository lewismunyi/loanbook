<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statements', function (Blueprint $table) {
            //select b.LOANSERIALNO,b.txt,b.AMOUNTCURDEBIT,b.AMOUNTCURCREDIT,b.TRANSDATE,b.period,0 as penalty,b.ACCOUNTNUM as accountnum from ledgerjournaltrans
            $table->increments('id');
            $table->string('serial_num');
            $table->bigInteger('rec_id')->unique();
            $table->double('debit_amount')->nullable();
            $table->double('credit_amount')->nullable();
            $table->date('transaction_date')->nullable();
            $table->string('transaction_period')->nullable();
            $table->integer('transaction_type')->nullable(); //1=principal balance 2=loan to institution 3=attachement fee 4=principal repayment 5=intrest charge 6=interest paid 7=penalty charged 8=penalty paid 9=ledger fee 11=penalty waiver 12=loan waiver 13=insurance charge 14= insurance paid
            $table->string('transaction_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statements');
    }
}
