<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return redirect()->route('home');
});

// Route::get('/verifyemail/{token}', function(){
// 	auth()->logout();
// 	return 'you are verified';
// });

//Route::get('/verifyemail/{token}','Auth\RegisterController@verifyEmail');


Auth::routes();

Route::get('/landingpage', [
    'uses' => 'HomeController@landing',
    'as' => 'landingpage',
    'middleware' => 'auth'
]);

Route::get('/report-page', [
    'uses' => 'ReportsController@reportPage',
    'as' => 'report-page',
    'middleware' => 'auth'
]);

Route::post('/report', [
    'uses' => 'ReportsController@getReport',
    'as' => 'report',
    'middleware' => 'auth'
]);

Route::get('/addLoan', [
    'uses' => 'LoanController@addLoan',
    'as' => 'addLoan',
    'middleware' => 'auth'
]);

Route::get('/ageing', [
    'uses' => 'LoanController@ageing',
    'as' => 'ageing',
    'middleware' => 'auth'
]);

Route::post('/addLoan', [
    'uses' => 'LoanController@createNewLoan',
    'as' => 'addLoan',
    'middleware' => 'auth'
]);

Route::post('/upload', [
    'uses' => 'LoanController@uploadCsv',
    'as' => 'upload',
    'middleware' => 'auth'
]);

Route::get('/home', [
	'uses' => 'HomeController@index',
	'as' => 'home',
	'middleware' => 'auth'
]);

Route::get('/test', function(){
    return view('test');
});

Route::post('/home', [
	'uses' => 'HomeController@filterResults',
	'as' => 'home',
	'middleware' => 'auth'
]);

Route::get('/loanbook', [
	'uses' => 'LoanController@loanbook',
	'as' => 'loanbook',
	'middleware' => 'auth'
]);

Route::get('/statements', [
	'uses' => 'LoanController@statements',
	'as' => 'statements',
	'middleware' => 'auth'
]);
Route::get('/statement', [
	'uses' => 'LoanController@statement',
	'as' => 'statement',
	'middleware' => 'auth'
]);



Route::post('/ageingresults', [
	'uses' => 'LoanController@loanAgeingResults',
	'as' => 'ageingresults',
	'middleware' => 'auth'
]);

Route::get('/ageingcsv', [
    'uses' => 'LoanController@ageingCsv',
    'as' => 'ageingcsv',
    'middleware' => 'auth'
]);

Route::get('/ageingpdf', [
    'uses' => 'LoanController@ageingPdf',
    'as' => 'ageingpdf',
    'middleware' => 'auth'
]);


Route::get('/clearanceprojection', [
	'uses' => 'LoanController@clearanceProjection',
	'as' => 'clearanceprojection',
	'middleware' => 'auth'
]);

Route::post('/clearanceprojectionresults', [
	'uses' => 'LoanController@clearanceProjectionResults',
	'as' => 'clearanceprojectionresults',
	'middleware' => 'auth'
]);



Route::post('/create-statement-lines', [
	'uses' => 'StatementsController@createStatementLines',
	'as' => 'create-statement-lines',
	'middleware' => 'auth'
]);


Route::post('/collections-report', [
	'uses' => 'ReportsController@collectionsReportFilter',
	'as' => 'collections-report',
	'middleware' => 'auth'
]);


Route::get('/collections-report', [
	'uses' => 'ReportsController@collectionsReport',
	'as' => 'collections-report',
	'middleware' => 'auth'
]);


Route::post('/billing-report', [
	'uses' => 'ReportsController@billingReportFilter',
	'as' => 'billing-report',
	'middleware' => 'auth'
]);


Route::get('/billing-report', [
	'uses' => 'ReportsController@billingReport',
	'as' => 'billing-report',
	'middleware' => 'auth'
]);

Route::get('/billingreportcron', [
	'uses' => 'ReportsController@billingReportcron',
	'as' => 'billingreportcron',
	'middleware' => 'auth'
]);

Route::get('/checkoffcron', [
	'uses' => 'ReportsController@checkoffcron',
	'as' => 'checkoffcron',
	'middleware' => 'auth'
]);


Route::get('/p_d_s/{accountnum}', [
	'uses' => 'StatementsController@printdetailedstatement',
	'as' => 'p_d_s',
	'middleware' => 'auth'
]);


Route::get('/p_s_s/{accountnum}', [
	'uses' => 'StatementsController@printsummarystatement',
	'as' => 'p_s_s',
	'middleware' => 'auth'
]);



Route::get('/loans', [
	'uses' => 'LoanController@loans',
	'as' => 'loans',
	'middleware' => 'auth'
]);

Route::get('/loansperyear', [
	'uses' => 'LoanController@loansperyear',
	'as' => 'loansperyear',
	'middleware' => 'auth'
]);

Route::post('/loans-filter', [
	'uses' => 'LoanController@LoansFIlter',
	'as' => 'loans-filter',
	'middleware' => 'auth'
]);

Route::get('/loans-default', [
	'uses' => 'LoanController@defaultloans',
	'as' => 'default-loans',
	'middleware' => 'auth'
]);

Route::post('/loans-default-filter', [
	'uses' => 'LoanController@defaultLoansFIlter',
	'as' => 'default-loans-filter',
	'middleware' => 'auth'
]);

Route::get('/cleared-loans', [
	'uses' => 'LoanController@clearedloans',
	'as' => 'cleared-loans',
	'middleware' => 'auth'
]);
Route::post('/cleared-loans', [
	'uses' => 'LoanController@clearedloansfiltered',
	'as' => 'cleared-loans',
	'middleware' => 'auth'
]);

Route::get('/cleared-loans-filter)', [
	'uses' => 'LoanController@clearedLoansFIlter',
	'as' => 'cleared-loans-filter',
	'middleware' => 'auth'
]);

Route::get('/dormant-loans', [
	'uses' => 'LoanController@dormantloans',
	'as' => 'dormant-loans',
	'middleware' => 'auth'
]);

Route::post('/dormantfilter', [
	'uses' => 'LoanController@dormantfilter',
	'as' => 'dormantfilter',
	'middleware' => 'auth'
]);
Route::post('/unmaturedfilter', [
	'uses' => 'LoanController@unmaturedfilter',
	'as' => 'unmaturedfilter',
	'middleware' => 'auth'
]);

Route::post('/maturedfiltered', [
	'uses' => 'LoanController@maturedfiltered',
	'as' => 'maturedfiltered',
	'middleware' => 'auth'
]);

Route::get('/maturedfilter', [
	'uses' => 'LoanController@maturedfilter',
	'as' => 'maturedfilter',
	'middleware' => 'auth'
]);


Route::post('/dormant-loans-filter', [
	'uses' => 'LoanController@dormantLoansFIlter',
	'as' => 'dormant-loans-filter',
	'middleware' => 'auth'
]);

Route::get('/performing-loans', [
	'uses' => 'LoanController@performingloans',
	'as' => 'performing-loans',
	'middleware' => 'auth'
]);

Route::post('/performing-loans-filter', [
	'uses' => 'LoanController@performingLoansFIlter',
	'as' => 'performing-loans-filter',
	'middleware' => 'auth'
]);

Route::get('/get-loans', [
	'uses' => 'LoanController@getLoans',
	'as' => 'get-loans',
	'middleware' => 'auth'
]);


Route::get('/get-summary', [
	'uses' => 'LoanController@getSummary',
	'as' => 'get-summary',
	'middleware' => 'auth'
]);

Route::get('/fetch-loans', [
	'uses' => 'LoanController@createLoan',
	'as' => 'fetch-loans',
	'middleware' => 'auth'
]);
Route::get('/fetch-loans-summed', [
	'uses' => 'LoanssummedController@createLoansummed',
	'as' => 'fetch-loans-summed',
	'middleware' => 'auth'
]);

Route::get('/fetch-loan-statements', [
	'uses' => 'LoanController@fetchLoanStatements',
	'as' => 'fetch-loan-statements',
	'middleware' => 'auth'
]);
Route::get('/detailed-statement', [
	'uses' => 'LoanController@detailedstatement',
	'as' => 'detailed-statement',
	'middleware' => 'auth'
]);
Route::get('/update-transdate', [
	'uses' => 'LoanController@transDateUpdateOnLoans2',
	'as' => 'update-transdate',
	'middleware' => 'auth'
]);
Route::get('/update-transdatea', [
	'uses' => 'LoanController@transDateUpdateOnLoans2a',
	'as' => 'update-transdatea',
	'middleware' => 'auth'
]);
Route::get('/update-transdateb', [
	'uses' => 'LoanController@transDateUpdateOnLoans2b',
	'as' => 'update-transdateb',
	'middleware' => 'auth'
]);
Route::get('/update-transdatec', [
	'uses' => 'LoanController@transDateUpdateOnLoans2c',
	'as' => 'update-transdatec',
	'middleware' => 'auth'
]);
Route::get('/update-transdated', [
	'uses' => 'LoanController@transDateUpdateOnLoans2d',
	'as' => 'update-transdated',
	'middleware' => 'auth'
]);
Route::get('/update-transdatee', [
	'uses' => 'LoanController@transDateUpdateOnLoans2e',
	'as' => 'update-transdatee',
	'middleware' => 'auth'
]);
Route::get('/update-transdatec', [
	'uses' => 'LoanController@transDateUpdateOnLoans2c',
	'as' => 'update-transdatec',
	'middleware' => 'auth'
]);
Route::get('/update-transdatef', [
	'uses' => 'LoanController@transDateUpdateOnLoans2f',
	'as' => 'update-transdatef',
	'middleware' => 'auth'
]);
Route::get('/update-transdateg', [
	'uses' => 'LoanController@transDateUpdateOnLoans2g',
	'as' => 'update-transdateg',
	'middleware' => 'auth'
]);
Route::get('/update-transdateh', [
	'uses' => 'LoanController@transDateUpdateOnLoans2h',
	'as' => 'update-transdateh',
	'middleware' => 'auth'
]);
Route::get('/update-transdatei', [
	'uses' => 'LoanController@transDateUpdateOnLoans2i',
	'as' => 'update-transdatei',
	'middleware' => 'auth'
]);

Route::get('/fetch-loan-statements3', [
	'uses' => 'LoanController@fetchLoanStatements3',
	'as' => 'fetch-loan-statements3',
	'middleware' => 'auth'
]);
Route::get('/fetch-loan-statements4', [
	'uses' => 'LoanController@fetchLoanStatements4',
	'as' => 'fetch-loan-statements4',
	'middleware' => 'auth'
]);
Route::get('/fetch-loan-statements5', [
	'uses' => 'LoanController@fetchLoanStatements5',
	'as' => 'fetch-loan-statements5',
	'middleware' => 'auth'
]);
Route::get('/fetch-loan-statements6', [
	'uses' => 'LoanController@fetchLoanStatements6',
	'as' => 'fetch-loan-statements6',
	'middleware' => 'auth'
]);

Route::get('/loans-csv', [
	'uses' => 'LoanController@largeCSV',
	'as' => 'loans-csv',
	'middleware' => 'auth'
]);
Route::get('/loansperyear-csv', [
	'uses' => 'LoanController@loansperyearCSV',
	'as' => 'loansperyear-csv',
	'middleware' => 'auth'
]);
Route::get('/loansperquarter-csv', [
	'uses' => 'LoanController@loansperquarterCSV',
	'as' => 'loansperquarter-csv',
	'middleware' => 'auth'
]);

Route::get('/loansperquarter', [
	'uses' => 'LoanController@loansperquarter',
	'as' => 'loansperquarter',
	'middleware' => 'auth'
]);


Route::get('/performing-loans-csv', [
    'uses' => 'LoanController@performingLoansCsv',
    'as' => 'performing-loans-csv',
    'middleware' => 'auth'
]);

Route::get('/cleared-loans-csv', [
    'uses' => 'LoanController@clearedLoansCsv',
    'as' => 'cleared-loans-csv',
    'middleware' => 'auth'
]);
Route::post('/cleared-loans-csv', [
    'uses' => 'LoanController@clearedLoansCsv',
    'as' => 'cleared-loans-csv',
    'middleware' => 'auth'
]);
Route::post('/loansfiltered', [
    'uses' => 'LoanController@loansfiltered',
    'as' => 'loansfiltered',
    'middleware' => 'auth'
]);

Route::get('/unmatured-loans-csv', [
    'uses' => 'LoanController@unmaturedLoansCsv',
    'as' => 'unmatured-loans-csv',
    'middleware' => 'auth'
]);

Route::get('/unmatured', [
    'uses' => 'LoanController@unmatured',
    'as' => 'unmatured',
    'middleware' => 'auth'
]);

Route::get('/performing-loans-pdf', [
    'uses' => 'LoanController@performingLoansPdf',
    'as' => 'performing-loans-pdf',
    'middleware' => 'auth'
]);

Route::get('/dormant-loans-csv', [
    'uses' => 'LoanController@dormantLoansCsv',
    'as' => 'dormant-loans-csv',
    'middleware' => 'auth'
]);

Route::get('/loans-pdf', [
	'uses' => 'LoanController@loansPDF',
	'as' => 'loans-pdf',
	'middleware' => 'auth'
]);

Route::get('/summaries-pdf', [
	'uses' => 'LoanController@summariesPDF',
	'as' => 'summaries-pdf',
	'middleware' => 'auth'
]);

Route::get('/defaulters-csv', [
	'uses' => 'LoanController@defaultersCSV',
	'as' => 'defaulters-csv',
	'middleware' => 'auth'
]);


Route::get('/clearanceprojection-csv', [
	'uses' => 'LoanController@clearanceProjectionCSV',
	'as' => 'clearanceprojection-csv',
	'middleware' => 'auth'
]);


Route::get('/calculate-fields', [
	'uses' => 'LoanController@calculateLoanFields',
	'as' => 'calculate-fields',
	'middleware' => 'auth'
]);

Route::get('/calculate-interest-charged', [
	'uses' => 'LoanController@calculateInterestCharged',
	'as' => 'calculate-interest-charged',
	'middleware' => 'auth'
]);
