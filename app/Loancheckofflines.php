<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loancheckofflines extends Model
{
    
     public function createLines($arr,$header_id) {
     	//dd($arr->lines);
     if($arr){
     	
        foreach ($arr->lines as $key => $line) {
        	   $record = [
                            "recid"=>$line->RECID,
                            "loancheckoffheaders_id"=>$header_id,
                            "accountnum"=>$line->ACCOUNTNUM,
                            "nationalidno" => $line->NATIONALIDNO,
                            "amount" => number_format($line->AMOUNT, 2, '.', ''),
                            "amountexpected" => number_format($line->AMOUNTEXPECTED, 2, '.', ''),
                            "checkoffbatchno" => $line->CHECKOFFBATCHNO,
                            "comments" => $line->COMMENTS,
                            "documentdate" => $line->DOCUMENTDATE,
                            "documentno" => $line->DOCUMENTNO,
                            "found" => $line->FOUND,
                            "guarantor" => $line->GUARANTOR,
                       		"lastpaymentdate" => $line->LASTPAYMENTDATE
                        ];
                        
                        //$data[] = $record;
                        $this->insert($record);
      }
     // if(count($arr->lines)>0)
     // return $this->insert($data);

      }
     
    }

}
