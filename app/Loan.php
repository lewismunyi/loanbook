<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;
use App\LoanStatement;

class Loan extends Model
{
protected $table = 'loans2';
public $timestamps = false;
    protected $fillable = ['rec_id', 'username', 'account_num', 'id_no', 'loan_category', 'loan_category_code', 'loan_serial_num', 'academic_year', 'student_reg_num', 'university', 'institution_code', 'outstanding_balance', 'principal_disbursed', 'start_year', 'end_year', 'loan_disbursement_date', 'loan_maturity_date', 'is_matured', 'phone_num', 'email', 'statements_migration_status'];

    public function scopeMatured($query){
    	return $query->where('loan_maturity_date', '<=', Carbon::today());
    }

    public function scopeUnmatured($query){
        return $query->where('loan_maturity_date', '>', Carbon::today());
    }
    public function scopeCleared($query){
       $query->where('outstanding_balance','<=',0);
    }
    public function scopeRunning($query){
        return $query->where('outstanding_balance', '>', 0);
    }

     public function scopePerforming($query){
        // $query->where('lasttrans_date', '>', Carbon::now()->subDays(180)->toDateTimeString())
        //           ->where('outstanding_balance','>',0)
        //           ->orWhere('loan_maturity_date', '>', Carbon::today());

      $query->where('loan_maturity_date', '>', Carbon::today()->subYear())
            ->orWhere(function ($query) {
                $query->where('lasttrans_date', '>', Carbon::now()->subDays(180)->toDateTimeString())
                      ->where('outstanding_balance','>',0);
            });
    }

    public function scopeDefaulted($query){
      $query->where('lasttrans_date', '<', Carbon::now()->subDays(180)->toDateTimeString())
                  ->where('outstanding_balance','>',0)
                  ->where('lasttrans_date','!=','0000-00-00')
                  ->where('loan_maturity_date', '<', Carbon::today()->subYear());
                      }

    public function scopeDormantloans($query){
           $query->where('lasttrans_date','=','0000-00-00')
                  ->where('outstanding_balance','>',0)
                  ->where('loan_maturity_date', '<', Carbon::today()->subYear());
    }

 public function scopeNpl($query){
            $query->where('outstanding_balance','>',0)
                  ->where('lasttrans_date', '<', Carbon::now()->subDays(180)->toDateTimeString())
                  ->where('loan_maturity_date', '<', Carbon::today()->subYear());
                  
    }


    //Statements
    public function statementLines(){
        return $this->hasMany('App\LoanStatementDetailed','loan_statement_detailed_headers_id');
    }  
    public function lastprincipal(){
        return $this->hasOne('App\LoansLastPrincipalPaid','accountnum','account_num');
    }

    // public function scopeDormantloans($query){
    //     return $query->whereDoesntHave('lastprincipal')
    //                 ->where('outstanding_balance','>',0)
    //                 ->get();
    // }
    //repayments
    // public function principalPaid(){
    //     return $this->statements()->where('transaction_type', '=', 4)->sum('credit_amount');
    // }
}
