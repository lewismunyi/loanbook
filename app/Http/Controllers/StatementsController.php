<?php

namespace App\Http\Controllers;
use App\LoanStatement;
use Illuminate\Http\Request;
use App\Statement;
use App\LoanStatementDetailed;
use App\LoanStatementDetailedHeader;
use App\LoansLastPrincipalPaid;
use Carbon\Carbon;
use Auth;
use PDF;
use DB;


class StatementsController extends Controller
{
    public function index(){
    	$statements = Statement::principalRepayment()->get();
    	dd($statements);
    }

    public function createStatementLines(Request $request){

    	$this->validate($request, [
    		'idno' => 'required|numeric' ,   		
    	]);
       
	    $idno=$request["idno"];
        $loanStatement = new LoanStatement();
        $loanStatementDetailedHeader = new LoanStatementDetailedHeader();
        $arr=array('idno' =>$idno);
        $action='validloansview';
        $validloans =$loanStatement->doAPIcall($arr,$action);

if(count($validloans)){
         $loanStatementDetailedHeader->where('idnumber','=',$idno)->delete(); 
        foreach ($validloans as $key => $validloan) {
#create header valid loans
             $header_id= $loanStatementDetailedHeader->createHeader($validloan);
             
#----
                   $accountnum= $validloan->accountnum;
                    if (isset($request["updatebalances"]) && $request["updatebalances"]==1) {            
                    $arr=array('serialno' =>$validloan->loanserialno);
                    $action='computeoutstandingbalancesforAX';
                    $loanStatement->doAPIcall($arr,$action);
                    }
        
        LoanStatementDetailed::where('accountnum','=',$validloan->accountnum)->delete();     
        $arr=array('serial_num' => $validloan->loanserialno);
        $action='loanstatement';
        $statements = $loanStatement->doAPIcall($arr,$action);
        //dd($statements);
           if(count($statements)>0){
                    //$data = [];
                    foreach ($statements as $key => $statement) {
                      //  dd($statement);
            if ($statement->period!='' && strlen($statement->period) < 3) {

                          $periodss = explode(" ",$statement->period);
                          $mth = strtolower($periodss[0]);
                          $year =$periodss[1];
                          $mth = substr($mth , 0, 3);

                                           
                        switch ($mth) {
                            case 'jan'  : $month = 1 ; break;
                            case 'feb' : $month = 2; break;
                            case 'mar'    : $month = 3; break;
                            case 'apr'    : $month = 4; break;
                            case 'may'      : $month = 5; break;
                            case 'jun'     : $month = 6 ; break;
                            case 'jul'     : $month = 7; break;
                            case 'aug'   : $month = 8; break;
                            case 'sep': $month = 9; break;
                            case 'oct'  : $month = 10; break;
                            case 'nov' : $month = 11; break;
                            case 'dec' : $month = 12; break;
                        }
                         $time = strtotime('28-'.$month.'-'.$year);
                         $trans_period = date('Y-m-d',$time);


            }else{
                $trans_period =Carbon::parse($statement->TRANSDATE)->format('Y-m-d');
            }
                       


                        $penalty = $statement->penalty;
                        if($penalty>0){
                            $statement->AMOUNTCURDEBIT = $penalty;
                        }elseif($penalty<0){
                            $statement->AMOUNTCURCREDIT = $penalty*-1;
                        }
                        $record = [
                            "loan_statement_detailed_headers_id"=>$header_id,
                            "accountnum"=>$validloan->accountnum,
                            "serial_num" => $statement->LOANSERIALNO,
                            "journalnum" => $statement->JOURNALNUM,
                            "debit_amount" => $statement->AMOUNTCURDEBIT,
                            "credit_amount" => $statement->AMOUNTCURCREDIT,
                            "transaction_date" => Carbon::parse($statement->TRANSDATE)->format('Y-m-d'),
                            "transaction_period" => $statement->period,
                            "transaction_type" => (int)$statement->LOANTRANSACTIONTYPE,
                            "trans_period" => $trans_period,
                            "empcode"=>$statement->empcode,
                            "documentnum" => $statement->paymode=='Mpesa'?$statement->MPESA_ref:$statement->documentnum,
                            "paymode" => $statement->paymode,
                            "MPESA_ref" => $statement->MPESA_ref,
                            "transaction_description" => $statement->txt,
                            "originator" =>$statement->empcode=='0'?'HELB charges':$statement->name
                    
                        ];
                        //dd($record);
                        $data[] = $record;
                    }
                   // dd($data);
                     //LoanStatement::create($data);
                    
                    ////////////////////////////////////////////
                }
        }
        if (count($statements)>0) 
                LoanStatementDetailed::insert($data);
        $headers = $loanStatementDetailedHeader->where('accountnum', $accountnum)->with('statementLines')->orderBy('PRODUCTDESCRIPTION')->get();
         
            $lastpaydate = DB::table('loan_statement_detaileds')
                                                ->where('accountnum', $accountnum)
                                                ->where('credit_amount','>',0)
                                                ->max('transaction_date');
            $sum = DB::table('loan_statement_detailed_headers')
                                                ->where('accountnum', $accountnum)
                                                ->sum('running_balance');

          
             $status=''  ; 
    if ($sum<1) {
            $status='cleared'  ;
              }else{               
                     if ($lastpaydate>Carbon::now()->subDays(180)->toDateTimeString()) {
                    $status='performing' ;
                    }else{ 
                         $status='nonperforming' ;
                    }
            } 
 
        return view('statement')->with('data', Auth::user())->with('headers',$headers)->with('accountnum', $accountnum)->with('runningTotal',0)->with('status',$status)->with('idno',$idno);
	}else{
        $status='notfound' ;
         return view('statement')->with('data', Auth::user())->with('headers',[])->with('accountnum', 0)->with('runningTotal',0)->with('status',$status)->with('idno',$idno);
    }
 }



    public function printdetailedstatement($accountnum){
    $headers = LoanStatementDetailedHeader::where('accountnum', $accountnum)->orderBy('PRODUCTDESCRIPTION' ,'desc')->get();
               $pdf = PDF::loadView('pdf.statement', array('headers' => $headers,'runningTotal'=>0));
          
            return $pdf->stream('loanstatement.pdf');

    }
 public function printsummarystatement($accountnum){
    $headers = LoanStatementDetailedHeader::where('accountnum', $accountnum)->orderBy('PRODUCTDESCRIPTION' ,'desc')->get();
               $pdf = PDF::loadView('pdf.summarystatement', array('headers' => $headers,'runningTotal'=>0))->setPaper('a4');
           
            return $pdf->stream('loanstatement.pdf');

    }

    //public function Search all statements summary date
    public function dateFilter(Request $request){
    //$request(from)
    $from = $request->input('startDate');

    //$request(ToDate)
    $to = $request->input('endDate');
//    dd($from, $to);
        return view('collectionreport');
    //
    //Select all statements where transaction date falls between the above
    //$loans = LoanStatement::whereBetween('transaction_date', [$from, $to])->get();
    //loop through all the statements and find sum of all $loan_statement->debit amount
    //$totalCredit =

    //$totalDebit =
    //credit statement
    ///$values = [$totalCredit, $totalDebit]
    //return(view->with(json[$values]));
    }
}
