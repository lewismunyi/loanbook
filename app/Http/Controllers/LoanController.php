<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use function MongoDB\BSON\toJSON;
use Symfony\Component\HttpFoundation\StreamedResponse;
use App\Statistic;
use App\Loan;
use App\Loansperyear;
use App\Loansperquarter;
use App\newLoan;
use App\LoanStatement;
use App\LoansLastPrincipalPaid;
use App\loanProductType;
use App\Institution;
use Carbon\Carbon;
use DB;
use Response;
use App\User;
use Auth;
use PDF;

class LoanController extends Controller
{

    public function loans(){
        // $loans = Loan::select('academic_year', DB::raw('count(*) as total'))
  //                ->groupBy('academic_year')
  //                ->pluck('total','academic_year')->all();
        $loans = Loan::orderBy('rec_id', 'DESC')->paginate(20);

        //check if user requests to print all Loans
        //Currently, post request, fetch data AGAIN
        //Write json that achieves the same functionality
        //If value changes, pass the value to controller to
        //trigger
        // if($request->input(printPDF == 'true')){
        //  // // Send data to the view using loadView function of PDF facade
        //  $loansPDF = $loans->toArray();
        //  $pdf = PDF::loadView('loans', compact('loansPDF'));
        //
        //  // Finally, you can download the file using download function
        //  return $pdf->stream();
        // }

        return view('loans')->with('loans', $loans);
    }
  public function loansperyear(){
            $loans = Loansperyear::orderBy('rec_id', 'DESC')->paginate(20);
        return view('loansperyear')->with('loans', $loans);
    }

   public function loansperquarter(){
            $loans = Loansperquarter::orderBy('rec_id', 'DESC')->paginate(20);
        return view('loansperquarter')->with('loans', $loans);
    }

    public function LoansFilter(Request $request){
        $loanproduct = request('loanproduct');
        $dates = explode("to", preg_replace('/\s+/', '', request('datefilter')));
        $from = Carbon::createFromFormat('d/m/Y', $dates[0]);
        $to = Carbon::createFromFormat('d/m/Y', $dates[1]);
        $loans = Loan::whereBetween('loan_maturity_date', [$from, $to])
                    ->where('loan_category_code', $loanproduct)->paginate(20);
        // dd($loans->toArray());
        return view('loans')->with('loans', $loans);
    }

    public function defaultloans(){
        $loans = Loan::defaulted()->orderBy('rec_id', 'DESC')->paginate(20);
        return view('defaultloans')->with('loans', $loans);
    }
 public function unmatured(){
        $loans = Loan::unmatured()->orderBy('rec_id', 'DESC')->paginate(20);
        return view('unmaturedtloans')->with('loans', $loans);
    }

    public function defaultLoansFilter(Request $request){
        $loanproduct = request('loanproduct');
        $dates = explode("to", preg_replace('/\s+/', '', request('datefilter')));
        $from = Carbon::createFromFormat('d/m/Y', $dates[0]);
        $to = Carbon::createFromFormat('d/m/Y', $dates[1]);
        $loans = Loan::whereBetween('loan_maturity_date', [$from, $to])
                    ->where('loan_category_code', $loanproduct)->paginate(20);
        return view('defaultloans')->with('loans', $loans);
    }
 
   public function dormantloans(){
        $loans = Loan::dormantloans()->orderBy('username', 'ASC')->paginate(20);
//      echo(json_encode($loans));
        return view('dormantLoans')->with('loans', $loans);
    }

    public function dormantLoansFilter(Request $request){
        $loanproduct = request('loanproduct');
        $dates = explode("to", preg_replace('/\s+/', '', request('datefilter')));
        $from = Carbon::createFromFormat('d/m/Y', $dates[0]);
        $to = Carbon::createFromFormat('d/m/Y', $dates[1]);
        $loans = Loan::whereBetween('loan_maturity_date', [$from, $to])
                    ->where('loan_category_code', $loanproduct)->paginate(20);
        // dd($loans->toArray());
        return view('dormantloans')->with('loans', $loans);
    }

    public function clearedloans(){
        $loans = Loan::cleared()->orderBy('rec_id', 'DESC')->paginate(20);
        return view('clearedloans')->with('loans', $loans);
    }

    public function clearedLoansFilter(Request $request){
//        $loanproduct = request('loanproduct');
        
        $from = Carbon::createFromFormat('d/m/Y', $request['from']);
        $to = Carbon::createFromFormat('d/m/Y', $request['to']);
//
        $loans = Loan::whereBetween('loan_maturity_date', [$from, $to])
                    ->where('loan_category_code', $request['product'])->paginate(20);
//        dd($request['from']);
        // dd($loans->toArray());
//        return view('clearedloans')->with('loans', $loans);

        return response()->json($loans->toArray());
    }

    /*public function clearedLoansFilter(Request $request){
        $loanproduct = request('loanproduct');

        $from = Carbon::createFromFormat('d/m/Y', request('startDate'));
        $to = Carbon::createFromFormat('d/m/Y', request('endDate'));

        $loans = Loan::whereBetween('loan_maturity_date', [$from, $to])
            ->where('loan_category_code', $loanproduct)->paginate(20);
        // dd($loans->toArray());
        return view('clearedloans')->with('loans', $loans);
    }*/

    public function performingloans(){
        $loans = Loan::unmatured()->cleared()->orderBy('rec_id', 'DESC')->paginate(20);
        return view('performingloans')->with('loans', $loans);
    }

    public function performingLoansFilter(Request $request){
        $loanproduct = request('loanproduct');
        $dates = explode("to", preg_replace('/\s+/', '', request('datefilter')));
        $from = Carbon::createFromFormat('d/m/Y', $dates[0]);
        $to = Carbon::createFromFormat('d/m/Y', $dates[1]);
        $loans = Loan::whereBetween('loan_maturity_date', [$from, $to])
                    ->where('loan_category_code', $loanproduct)->paginate(20);
        // dd($loans->toArray());
        return view('performingloans')->with('loans', $loans);
    }

    public function clearanceprojection(){
         $results= array();
        return view('clearanceprojection')->with('results', $results);
    }

public function clearanceprojectionresults(Request $request){
       $this->validate($request, [
            'noofmonths' => 'required|numeric' ,
        ]);
       $noofmonths = request('noofmonths');
       $amount =3000*$noofmonths;
       $results = Loan::whereBetween('outstanding_balance', array(1, $amount))->get();
       $month=$noofmonths==1?'month':'months';
        return view('clearanceprojection')->with('results', $results)->with('noofmonths',$noofmonths)->with('month',$month);

    }

    public function statements(){
        return view('statements');
    }

    public function addloan(){
        $institutions = Institution::select('institutionname')->get();
        $loanproducttypes = loanProductType::select('productdescription')->get();

        return view('addLoan', compact('institutions', 'loanproducttypes'));
    }

    public function ageing(){
        $params = [
            'ageing' => '',
            'status' => ''
           ];
        $results = [];
        //return view('ageing')->with('results', $results);
        return view('ageing')->with('results', $results)->with('params', (object)$params);
    }

    public function ageingCsv(Request $request){
       //echo "Getting ageing CSV";
        dd($request->all());


    }

    public function ageingPdf(){
        echo "Getting ageing PDF";
    }

    public function loanAgeingResults(Request $request){

       $this->validate($request, [
            'ageing' => 'required|numeric' ,
            'loanstatus' => 'required' ,
        ]);

       $params = [
        'ageing' => request('ageing'),
        'status' => request('loanstatus')
       ];

       $val = request('ageing');
       $loanstatus = request('loanstatus');
       $currentYear = Carbon::now()->year;
       $r=$currentYear-$val;
        $results = [];
        

       if ($loanstatus=='All Loans'){
            $results = Loan::whereYear('loan_maturity_date', '=',  $r)->paginate(20);
       }elseif ($loanstatus=='Cleared'){
            $results = Loan::cleared()->whereYear('loan_maturity_date', '=',  $r)->paginate(20);
       }elseif ($loanstatus=='Dormant') {
           $results = Loan::dormantloans()->whereYear('loan_maturity_date', '=',  $r)->paginate(20);
       }elseif ($loanstatus=='Default') {
            $results = Loan::defaulted()->whereYear('loan_maturity_date', '=',  $r)->paginate(20);
       } elseif ($loanstatus=='Performing') {
            $results = Loan::Performing()->whereYear('loan_maturity_date', '=',  $r)->paginate(20);
       }


       //return view('ageing')->with('results', $results)->with('val',$val)->with('loanstatus',$loanstatus);
    //   return view('ageing', compact('results', 'val', 'loanstatus'));


       return view('ageing')->with('results', $results)->with('val',$val)
       ->with('loanstatus',$loanstatus)->with('params', (object)$params);

    }



    public function createNewLoan(Request $request){
        $loan = new newLoan;
        $loan->name = request('name');
        $loan->id_no = request('id_no');
        $loan->reg_no = request('reg_no');
        $loan->serial_no = request('serial_no');
        $loan->academic_year = request('academic_year');
        $loan->principle_loan = request('principle_loan');
        $loan->institution = request('institution');
        $loan->year_of_completion = request('year_of_completion');
       // dd(json_encode($loan));
       $loan->save();
             return redirect('/home');
    }

    public function getLoans(Request $request){
        $idno = $request->input('idno');
        //validate the ID Number here

        //Get loanee loans from AX
        $arr=array('idno' =>$idno);
        $action='getloaneepersonaldet';
        $loans = $this->doAPIcall($arr,$action);
        return response()->json([
            'name' => 'Peter Wanyonyi',
            'loans' => $loans
        ]);

        //return $idno;
    }
    public function statement(Request $request){
        $idno = $request->input('idno');
        $headers= array();
        $status= array();
        return view('statement')->with('data', Auth::user())->with('headers',$headers)->with('status',$status)->with('idno',$idno);
    }
     public function detailedstatement(Request $request){
        $idno = $request->input('idno');
        dd($idno);
        //Get loanee loans from AX
        $arr=array('idno' =>$idno);
        $action='getloaneepersonaldet';
        $loans = $this->doAPIcall($arr,$action);
        return response()->json([
            'name' => 'Peter Wanyonyi',
            'loans' => $loans
        ]);

        //return $idno;
    }


    public function getSummary(Request $request){
        $idno = $request->input('idno');
        //validate the ID Number here

        //Get loanee loans from AX
        $arr=array('accountnum' =>'10609469');
        $action='getsummarystatement';
        $loans = $this->doAPIcall($arr,$action);
        dd($loans);

        //$loans = Loan::where('account_num', '10609469')->skip(0)->limit(800)->get();

        // return response()->json([
        //  'name' => 'Peter Wanyonyi',
        //  'loans' => $loans
        // ]);
        return response()->json([
            'name' => 'Peter Wanyonyi',
             'loans' => $loans
             //[
            //  ['date' => '02-18-2017', 'refno'=>'REFMCDSR67', 'description'=>'this is a description', 'period'=>'Feb 2017','debit'=>'2200.02', 'credit'=>'0', 'runningbalance'=>'230674.70'],
            //  ['date' => '02-18-2017', 'refno'=>'REFMCDSR67', 'description'=>'this is a description', 'period'=>'Feb 2017','debit'=>'2200.02', 'credit'=>'0', 'runningbalance'=>'230674.70']
            // ]
        ]);

        //return $idno;
    }

    public function loanbook(){
        $stats = Statistic::first();
        return view('loanbook')->with('stats', $stats);
    }


    public function createLoan(){
        //get the last rec_id
        $last = Loan::orderBy('rec_id', 'DESC')->first();
        $id = isset($last->rec_id) ? $last->rec_id:0;
        //dd($id);
        //get the data from AX
        $arr=array('rec_id' => $id, 'viewname' => 'loandt');
        $action='callview';
        //$action='getloaneepersonaldet';
        $loans = $this->doAPIcall($arr,$action);
       // dd($loans);
        if(count($loans)){
        foreach ($loans as $key => $loan) {

            $record = [
                "rec_id" => $loan->rec_id,
                "username" => $loan->username,
                "account_num" => $loan->account_num,
                "id_no" => $loan->id_no,
                "loan_category" => $loan->loan_category,
                "loan_category_code" => $loan->loan_category_code,
                "loan_serial_num" => $loan->loan_serial_num,
                "academic_year" => $loan->academic_year,
                "student_reg_num" => $loan->student_reg_num,
                "university" => $loan->university,
                'institution_code' => $loan->institution_code,
                "outstanding_balance" => $loan->outstanding_balance,
                "principal_disbursed" => $loan->principal_disbursed,
                //"principal_disbursed" => $loan->principal_disbursed,
                //'total_amount_paid' => $loan->total_amount_paid,
                'start_year' => $loan->start_year,
               // 'end_year' => Carbon::parse($loan->end_year)->format('Y'),
                'loan_disbursement_date' => $loan->disbursementdate,
                'loan_maturity_date' => $loan->loan_maturity_date,
                'is_matured' => $loan->is_matured,
                'phone_num' => $loan->phone_num,
                'email' => $loan->email,
                'statements_migration_status' => 0
            ];
            Loan::create([$record]);
        }
    }
        //$data = $this->stdObjectToArray($loans);
        //dd($data);
        //save the data to the db
        //Loan::insert($data);
        return 'Done';

    }
/***
used to fetch mass records for loanbook
donot use for individual loan statements
----------------------------------------
    public function fetchLoanStatements(){
        //get the loans where statements have not been updated
        $loans = Loan::where('statements_migration_status', 0)->skip(0)->limit(80000)->get();
        if(count($loans)){
            foreach ($loans as $key => $loan) {
                /////////////////////////////////
                $serial_num = $loan->loan_serial_num;
                $arr=array('serial_num' => $serial_num, 'viewname' => 'detailedloanstatement1');
                $action='callview2';
                $statements = $this->doAPIcall($arr, $action);
                //dd($statements);
                if(count($statements)){
                    $data = [];
                    foreach ($statements as $key => $statement) {
                        $penalty = $statement->penalty;
                        if($penalty>0){
                            $statement->AMOUNTCURDEBIT = $penalty;
                        }elseif($penalty<0){
                            $statement->AMOUNTCURCREDIT = $penalty*-1;
                        }
                        $record = [
                            "loan_id" => $loan->id,
                            "serial_num" => $statement->LOANSERIALNO,
                            "rec_id" => $statement->RECID,
                            "debit_amount" => $statement->AMOUNTCURDEBIT,
                            "credit_amount" => $statement->AMOUNTCURCREDIT,
                            "transaction_date" => Carbon::parse($statement->TRANSDATE)->format('Y-m-d'),
                            "transaction_period" => $statement->period,
                            "transaction_type" => (int)$statement->LOANTRANSACTIONTYPE,
                            "transaction_description" => $statement->txt
                        ];
                        //dd($record);
                        if(!LoanStatement::where('rec_id', '=', $statement->RECID)->exists()){
                            LoanStatement::create($record);
                        }
                    }
                    ////////////////////////////////////////////
                }
                //update the loan migration details
                $loan->statements_migration_status = 1;
                $loan->statements_migration_date = Carbon::now()->format('Y-m-d');
                $loan->save();
                //////////////////////////////////////
            }

        }
    }
-----------------------
*/
function csvToArray($filename, $delimiter)
{
//    if (!file_exists($filename) || !is_readable($filename))
//        return false;

    $header = null;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== false)
    {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
        {
            if (!$header)
                $header = $row;
            else
                $data[] = array_combine($header, $row);
        }
        fclose($handle);
    }

    return $data;
}
public function uploadCsv(Request $request){
    $file = $request->file('uploadfield');
    $name = Auth::user()->name.'.'.$file->getClientOriginalExtension();
    dd($name);
    $file = $request->uploadfield;


    dd($file);
//    $file = Input::file('uploadfield')->getClientOriginalName();
//    dd($file);
//    if ($request->hasFile('uploadfield')) {
//        dd("Helo if");
//    }


//    $new_name = 'ourcsv.' . $file->getClientOriginalExtension();
//    dd($new_name);
//    $file->getClientOriginalExtension();
    $data = $this->csvToArray($file, ",");

//    dd($data);
//    dd($file);
}

 public function transDateUpdateOnLoans2(){

        $loanStatement = new LoanStatement;
        $loans = Loan::whereBetween('id', array(1100000, 1150000))
                        ->where('lasttrans_date','0000-00-00')
                        //->where('loan_maturity_date', '<', Carbon::today())
                        ->where('outstanding_balance','>',0)                        
                        ->select('id', 'account_num')
                        ->get();

        if(count($loans)){
            foreach ($loans as $key => $loan) {

                $tarehe = DB::table('loans_last_principal_paid')->where('accountnum', $loan->account_num)->value('lasttrans_date');
                 //dd($loan);
                            if($tarehe!=''){ 
                            $loan->lasttrans_date =  $tarehe;
                           // dd($loan);
                            $loan->save();
                            }
                        }
                    }

                }

 public function transDateUpdateOnLoans2a(){

        
        $loanStatement = new LoanStatement;
        $loans = Loan::whereBetween('id', array(1150000, 1200000))
                        ->where('lasttrans_date','0000-00-00')
                        ->where('outstanding_balance','>',0)                        
                        ->select('id', 'account_num')
                        ->get();
//dd($loans);
        if(count($loans)){
            foreach ($loans as $key => $loan) {
                $tarehe = DB::table('loans_last_principal_paid')->where('accountnum', $loan->account_num)->value('lasttrans_date');
              //  echo $tarehe;
                            if($tarehe!=''){ 
                            $loan->lasttrans_date =  $tarehe;
                            //dd($loan);
                            $loan->save();
                            }
                        }
                    }
}
 public function transDateUpdateOnLoans2b(){

       
        $loanStatement = new LoanStatement;
        $loans = Loan::whereBetween('id', array(1200000, 1250000))
                        ->where('lasttrans_date','0000-00-00')
                        ->where('outstanding_balance','>',0)                        
                        ->select('id', 'account_num')
                        ->get();
//dd($loans);
        if(count($loans)){
            foreach ($loans as $key => $loan) {
                $tarehe = DB::table('loans_last_principal_paid')->where('accountnum', $loan->account_num)->value('lasttrans_date');
                            if($tarehe!=''){ 
                            $loan->lasttrans_date =  $tarehe;
                            //dd($loan);
                            $loan->save();
                            }
                        }
                    }

                }


 public function transDateUpdateOnLoans2c(){

       
        $loanStatement = new LoanStatement;
        $loans = Loan::whereBetween('id', array(1250000, 1300000))
                        ->where('lasttrans_date','0000-00-00')
                        ->where('outstanding_balance','>',0)                        
                        ->select('id', 'account_num')
                        ->get();
//dd($loans);
        if(count($loans)){
            foreach ($loans as $key => $loan) {
                $tarehe = DB::table('loans_last_principal_paid')->where('accountnum', $loan->account_num)->value('lasttrans_date');
                            if($tarehe!=''){ 
                            $loan->lasttrans_date =  $tarehe;
                            //dd($loan);
                            $loan->save();
                            }
                        }
                    }

                }

 public function transDateUpdateOnLoans2d(){

      
        $loanStatement = new LoanStatement;
        $loans = Loan::whereBetween('id', array(1300000, 1350000))
                        ->where('lasttrans_date','0000-00-00')
                        ->where('outstanding_balance','>',0)                        
                        ->select('id', 'account_num')
                        ->get();
//dd($loans);
        if(count($loans)){
            foreach ($loans as $key => $loan) {
                $tarehe = DB::table('loans_last_principal_paid')->where('accountnum', $loan->account_num)->value('lasttrans_date');
                            if($tarehe!=''){ 
                            $loan->lasttrans_date =  $tarehe;
                            //dd($loan);
                            $loan->save();
                            }
                        }
                    }

                }
 public function transDateUpdateOnLoans2e(){

       
        $loanStatement = new LoanStatement;
        $loans = Loan::whereBetween('id', array(1100000, 1150000))
                        ->where('lasttrans_date','0000-00-00')
                        ->where('outstanding_balance','>',0)                        
                        ->select('id', 'account_num')
                        ->get();
//dd($loans);
        if(count($loans)){
            foreach ($loans as $key => $loan) {
                $tarehe = DB::table('loans_last_principal_paid')->where('accountnum', $loan->account_num)->value('lasttrans_date');
                            if($tarehe!=''){ 
                            $loan->lasttrans_date =  $tarehe;
                            //dd($loan);
                            $loan->save();
                            }
                        }
                    }

                }

 public function transDateUpdateOnLoans2f(){

     
        $loanStatement = new LoanStatement;
        $loans = Loan::whereBetween('id', array(0, 5000))
                        ->where('lasttrans_date','0000-00-00')
                        ->where('outstanding_balance','>',0)                        
                        ->select('id', 'account_num')
                        ->get();
//dd($loans);
        if(count($loans)){
            foreach ($loans as $key => $loan) {
                $tarehe = DB::table('loans_last_principal_paid')->where('accountnum', $loan->account_num)->value('lasttrans_date');
                            if($tarehe!=''){ 
                            $loan->lasttrans_date =  $tarehe;
                            //dd($loan);
                            $loan->save();
                            }
                        }
                    }

                }

 public function transDateUpdateOnLoans2g(){

      
        $loanStatement = new LoanStatement;
        $loans = Loan::whereBetween('id', array(0, 5000))
                        ->where('lasttrans_date','0000-00-00')
                        ->where('outstanding_balance','>',0)                        
                        ->select('id', 'account_num')
                        ->get();
//dd($loans);
        if(count($loans)){
            foreach ($loans as $key => $loan) {
                $tarehe = DB::table('loans_last_principal_paid')->where('accountnum', $loan->account_num)->value('lasttrans_date');
                            if($tarehe!=''){ 
                            $loan->lasttrans_date =  $tarehe;
                            //dd($loan);
                            $loan->save();
                            }
                        }
                    }

                }

 public function transDateUpdateOnLoans2h(){

       
        $loanStatement = new LoanStatement;
        $loans = Loan::whereBetween('id', array(0, 5000))
                        ->where('lasttrans_date','0000-00-00')
                        ->where('outstanding_balance','>',0)                        
                        ->select('id', 'account_num')
                        ->get();
//dd($loans);
        if(count($loans)){
            foreach ($loans as $key => $loan) {
                $tarehe = DB::table('loans_last_principal_paid')->where('accountnum', $loan->account_num)->value('lasttrans_date');
                            if($tarehe!=''){ 
                            $loan->lasttrans_date =  $tarehe;
                            //dd($loan);
                            $loan->save();
                            }
                        }
                    }

                }

 public function transDateUpdateOnLoans2i(){

       
        $loanStatement = new LoanStatement;
        $loans = Loan::whereBetween('id', array(0, 5000))
                        ->where('lasttrans_date','0000-00-00')
                        ->where('outstanding_balance','>',0)                        
                        ->select('id', 'account_num')
                        ->get();
//dd($loans);
        if(count($loans)){
            foreach ($loans as $key => $loan) {
                $tarehe = DB::table('loans_last_principal_paid')->where('accountnum', $loan->account_num)->value('lasttrans_date');
                            if($tarehe!=''){ 
                            $loan->lasttrans_date =  $tarehe;
                            //dd($loan);
                            $loan->save();
                            }
                        }
                    }

                }

 public function detailedLoanStatements(Request $request){
        //get the loans where statements have not been updated
       $idno = $request->input('idno');


        $loans = Loan::where('statements_migration_status', 0)->skip(0)->limit(80000)->get();
        //$loans = Loan::where('statements_migration_status', 0)->skip(0)->limit(80000)->get();
        $loans = Loan::where('statements_migration_status', 0)
                        ->whereBetween('id', array(200001, 300000))
                        ->get();

        if(count($loans)){
            foreach ($loans as $key => $loan) {
                /////////////////////////////////
                $serial_num = $loan->loan_serial_num;
                $arr=array('serial_num' => $serial_num, 'viewname' => 'detailedloanstatement');
                $action='callview2';
                $statements = $this->doAPIcall($arr, $action);
                //dd($statements);
                if(count($statements)){
                    $data = [];
                    foreach ($statements as $key => $statement) {
                        $penalty = $statement->penalty;
                        if($penalty>0){
                            $statement->AMOUNTCURDEBIT = $penalty;
                        }elseif($penalty<0){
                            $statement->AMOUNTCURCREDIT = $penalty*-1;
                        }
                        $record = [
                            "loan_id" => $loan->id,
                            "serial_num" => $statement->LOANSERIALNO,
                            "rec_id" => $statement->RECID,
                            "debit_amount" => $statement->AMOUNTCURDEBIT,
                            "credit_amount" => $statement->AMOUNTCURCREDIT,
                            "transaction_date" => Carbon::parse($statement->TRANSDATE)->format('Y-m-d'),
                            "transaction_period" => $statement->period,
                            "transaction_type" => (int)$statement->LOANTRANSACTIONTYPE,
                            "transaction_description" => $statement->txt
                        ];
                        //dd($record);
                        if(!LoanStatement::where('rec_id', '=', $statement->RECID)->exists()){
                            LoanStatement::create($record);
                        }
                    }
                    ////////////////////////////////////////////
                }
                //update the loan migration details
                $loan->statements_migration_status = 1;
                $loan->statements_migration_date = Carbon::now()->format('Y-m-d');
                $loan->save();
                //////////////////////////////////////
            }

        }

    }

    public function performingLoansCsv(){
            $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');

            // Add CSV headers

            // 'SNo.', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'Principal', 'Principal Paid', 'Principal C/F', 'Int. Charged', 'Int. Paid', 'Int. C/F', 'Ldg. Charged', 'Ldg. Paid', 'Ldg. C/F', 'Ins. Charged', 'Ins Paid', 'Ins. C/F', 'Penalty Charged', 'Penalty Paid', 'Penalty C/F'

            fputcsv($handle, array('SNo.','Student Name','phone','email', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'university', 'Start Yr.', 'End Yr.', 'Maturity Date','Principal', 'Principal repaid', 'interest charged', 'interest repaid', 'ledger charged', 'ledger repaid', 'insurance charged', 'insurance repaid', 'penalty charged', 'penalty repaid','waivers','Running Bal'));
            //$loans = Loan::has('statements')->skip(0)->limit(500)->get();
            // Get all users

            Loan::performing()->chunk(5000, function($loans) use($handle){
            foreach ($loans as $loan) {
                // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->phone_num,
                    $loan->email,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->institution_code,
                    $loan->loan_category_code,
                    $loan->academic_year,
                    $loan->university,
                    $loan->start_year,
                   // $loan->end_year,
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    $loan->principal_repaid,
                    $loan->interest_charged,
                    $loan->interest_repaid,
                    $loan->ledger_charged,
                    $loan->ledger_repaid,
                    $loan->insurance_charged,
                    $loan->insurance_repaid,
                    $loan->penalty_charged,
                    $loan->penalty_repaid,
                    $loan->waivers_amount,
                    $loan->running_balance
                    //$loan->outstanding_balance
                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="performersList.csv"',
            ]);

        return $response->send();
    }

 
    public function performingLoansPdf(){
        dd("Exporting performing loans PDF");
//        return view('loanbook')->with('stats', $stats);
    }

    public function fetchLoanStatements3(){
        //get the loans where statements have not been updated
        //$loans = Loan::where('statements_migration_status', 0)->skip(0)->limit(80000)->get();
        $loans = Loan::where('statements_migration_status', 0)
                        ->whereBetween('id', array(300001, 400000))
                        ->get();

        if(count($loans)){
            foreach ($loans as $key => $loan) {
                /////////////////////////////////
                $serial_num = $loan->loan_serial_num;
                $arr=array('serial_num' => $serial_num, 'viewname' => 'detailedloanstatement1');
                $action='callview2';
                $statements = $this->doAPIcall($arr, $action);
                //dd($statements);
                if(count($statements)){
                    $data = [];
                    foreach ($statements as $key => $statement) {
                        $penalty = $statement->penalty;
                        if($penalty>0){
                            $statement->AMOUNTCURDEBIT = $penalty;
                        }elseif($penalty<0){
                            $statement->AMOUNTCURCREDIT = $penalty*-1;
                        }
                        $record = [
                            "loan_id" => $loan->id,
                            "serial_num" => $statement->LOANSERIALNO,
                            "rec_id" => $statement->RECID,
                            "debit_amount" => $statement->AMOUNTCURDEBIT,
                            "credit_amount" => $statement->AMOUNTCURCREDIT,
                            "transaction_date" => Carbon::parse($statement->TRANSDATE)->format('Y-m-d'),
                            "transaction_period" => $statement->period,
                            "transaction_type" => (int)$statement->LOANTRANSACTIONTYPE,
                            "transaction_description" => $statement->txt
                        ];
                        //dd($record);
                        if(!LoanStatement::where('rec_id', '=', $statement->RECID)->exists()){
                            LoanStatement::create($record);
                        }
                    }
                    ////////////////////////////////////////////
                }
                //update the loan migration details
                $loan->statements_migration_status = 1;
                $loan->statements_migration_date = Carbon::now()->format('Y-m-d');
                $loan->save();
                //////////////////////////////////////
            }

        }

    }

 public function fetchLoanStatements(){
        //get the loans where statements have not been updated
        //$loans = Loan::where('statements_migration_status', 0)->skip(0)->limit(80000)->get();
        $loans = Loan::where('statements_migration_status', 0)
                        ->whereBetween('id', array(400001, 500000))
                        ->get();

        if(count($loans)){
            foreach ($loans as $key => $loan) {
                /////////////////////////////////
                $serial_num = $loan->loan_serial_num;
                $arr=array('serial_num' => $serial_num, 'viewname' => 'detailedloanstatement1');
                $action='callview2';
                $statements = $this->doAPIcall($arr, $action);
                //dd($statements);
                if(count($statements)){
                    $data = [];
                    foreach ($statements as $key => $statement) {
                        $penalty = $statement->penalty;
                        if($penalty>0){
                            $statement->AMOUNTCURDEBIT = $penalty;
                        }elseif($penalty<0){
                            $statement->AMOUNTCURCREDIT = $penalty*-1;
                        }
                        $record = [
                            "loan_id" => $loan->id,
                            "serial_num" => $statement->LOANSERIALNO,
                            "rec_id" => $statement->RECID,
                            "debit_amount" => $statement->AMOUNTCURDEBIT,
                            "credit_amount" => $statement->AMOUNTCURCREDIT,
                            "transaction_date" => Carbon::parse($statement->TRANSDATE)->format('Y-m-d'),
                            "transaction_period" => $statement->period,
                            "transaction_type" => (int)$statement->LOANTRANSACTIONTYPE,
                            "transaction_description" => $statement->txt
                        ];
                        //dd($record);
                        if(!LoanStatement::where('rec_id', '=', $statement->RECID)->exists()){
                            LoanStatement::create($record);
                        }
                    }
                    ////////////////////////////////////////////
                }
                //update the loan migration details
                $loan->statements_migration_status = 1;
                $loan->statements_migration_date = Carbon::now()->format('Y-m-d');
                $loan->save();
                //////////////////////////////////////
            }

        }

    }



    public function fetchLoanStatements5(){
        //get the loans where statements have not been updated
        //$loans = Loan::where('statements_migration_status', 0)->skip(0)->limit(80000)->get();
        $loans = Loan::where('statements_migration_status', 0)
                        ->whereBetween('id', array(500001, 600000))
                        ->get();

        if(count($loans)){
            foreach ($loans as $key => $loan) {
                /////////////////////////////////
                $serial_num = $loan->loan_serial_num;
                $arr=array('serial_num' => $serial_num, 'viewname' => 'detailedloanstatement');
                $action='callview2';
                $statements = $this->doAPIcall1($arr, $action);
                //dd($statements);
                if(count($statements)){
                    $data = [];
                    foreach ($statements as $key => $statement) {
                        $penalty = $statement->penalty;
                        if($penalty>0){
                            $statement->AMOUNTCURDEBIT = $penalty;
                        }elseif($penalty<0){
                            $statement->AMOUNTCURCREDIT = $penalty*-1;
                        }
                        $record = [
                            "loan_id" => $loan->id,
                            "serial_num" => $statement->LOANSERIALNO,
                            "rec_id" => $statement->RECID,
                            "debit_amount" => $statement->AMOUNTCURDEBIT,
                            "credit_amount" => $statement->AMOUNTCURCREDIT,
                            "transaction_date" => Carbon::parse($statement->TRANSDATE)->format('Y-m-d'),
                            "transaction_period" => $statement->period,
                            "transaction_type" => (int)$statement->LOANTRANSACTIONTYPE,
                            "transaction_description" => $statement->txt
                        ];
                        //dd($record);
                        if(!LoanStatement::where('rec_id', '=', $statement->RECID)->exists()){
                            LoanStatement::create($record);
                        }
                    }
                    ////////////////////////////////////////////
                }
                //update the loan migration details
                $loan->statements_migration_status = 1;
                $loan->statements_migration_date = Carbon::now()->format('Y-m-d');
                $loan->save();
                //////////////////////////////////////
            }

        }

    }


    public function fetchLoanStatements6(){
        //get the loans where statements have not been updated
        //$loans = Loan::where('statements_migration_status', 0)->skip(0)->limit(80000)->get();
        $loans = Loan::where('statements_migration_status', 0)
                        ->whereBetween('id', array(600001, 700000))
                        ->get();

        if(count($loans)){
            foreach ($loans as $key => $loan) {
                /////////////////////////////////
                $serial_num = $loan->loan_serial_num;
                $arr=array('serial_num' => $serial_num, 'viewname' => 'detailedloanstatement');
                $action='callview2';
                $statements = $this->doAPIcall1($arr, $action);
                //dd($statements);
                if(count($statements)){
                    $data = [];
                    foreach ($statements as $key => $statement) {
                        $penalty = $statement->penalty;
                        if($penalty>0){
                            $statement->AMOUNTCURDEBIT = $penalty;
                        }elseif($penalty<0){
                            $statement->AMOUNTCURCREDIT = $penalty*-1;
                        }
                        $record = [
                            "loan_id" => $loan->id,
                            "serial_num" => $statement->LOANSERIALNO,
                            "rec_id" => $statement->RECID,
                            "debit_amount" => $statement->AMOUNTCURDEBIT,
                            "credit_amount" => $statement->AMOUNTCURCREDIT,
                            "transaction_date" => Carbon::parse($statement->TRANSDATE)->format('Y-m-d'),
                            "transaction_period" => $statement->period,
                            "transaction_type" => (int)$statement->LOANTRANSACTIONTYPE,
                            "transaction_description" => $statement->txt
                        ];
                        //dd($record);
                        if(!LoanStatement::where('rec_id', '=', $statement->RECID)->exists()){
                            LoanStatement::create($record);
                        }
                    }
                    ////////////////////////////////////////////
                }
                //update the loan migration details
                $loan->statements_migration_status = 1;
                $loan->statements_migration_date = Carbon::now()->format('Y-m-d');
                $loan->save();
                //////////////////////////////////////
            }

        }

    }

    //getrunning balance from ax
    public function doAPIcall($arr,$action) {

        //$url = "http://197.136.52.3:1924/esb.php?rquest=$action";
       // $url = "http://192.168.1.96:1924/esb.php?rquest=$action";
      $url = "http://localhost/dome/esb.php?rquest=$action";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($arr));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);
        //dd($response);
        return $response;
    }

public function doAPIcall1($arr,$action) {

        //$url = "http://197.136.52.3:1924/esb.php?rquest=$action";
        $url = "http://localhost:8081/dome/esb.php?rquest=$action";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($arr));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);
        //dd($response);
        return $response;
    }



    public function loansCSV(){
        $loans = Loan::has('statements')->skip(0)->limit(500)->get();
        $callback = function() use ($loans){
            $handle = fopen('php://output', 'w');
            fputcsv($handle, array('SNo.', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'Principal', 'Principal Paid', 'Principal C/F', 'Int. Charged', 'Int. Paid', 'Int. C/F', 'Ldg. Charged', 'Ldg. Paid', 'Ldg. C/F', 'Ins. Charged', 'Ins Paid', 'Ins. C/F', 'Penalty Charged', 'Penalty Paid', 'Penalty C/F'));
            foreach ($loans as $key => $loan) {
                //add line to excel
                $row = [
                    $loan->loan_serial_num,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->institution_code,
                    $loan->loan_category_code,
                    $loan->academic_year,
                    $loan->principal_disbursed,
                    $loan->statements()->principalrepayment()->sum('credit_amount'),
                    $loan->principal_disbursed - $loan->statements()->principalrepayment()->sum('credit_amount'),
                    $loan->statements()->interestcharged()->sum('debit_amount'),
                    $loan->statements()->interestpaid()->sum('credit_amount'),
                    $loan->statements()->interestcharged()->sum('debit_amount') - $loan->statements()->interestpaid()->sum('credit_amount'),
                    $loan->statements()->legerfeecharged()->sum('debit_amount'),
                    $loan->statements()->legerfeepaid()->sum('credit_amount'),
                    $loan->statements()->legerfeecharged()->sum('debit_amount') - $loan->statements()->legerfeepaid()->sum('credit_amount'),
                    $loan->statements()->insurancecharged()->sum('debit_amount'),
                    $loan->statements()->insurancepaid()->sum('credit_amount'),
                    $loan->statements()->insurancecharged()->sum('debit_amount') - $loan->statements()->insurancepaid()->sum('credit_amount'),
                    $loan->statements()->penaltycharged()->sum('debit_amount'),
                    $loan->statements()->penaltypaid()->sum('credit_amount'),
                    $loan->statements()->penaltycharged()->sum('debit_amount') - $loan->statements()->penaltypaid()->sum('credit_amount')
                ];
                fputcsv($handle, $row);
            }
            fclose($handle);
        };

        //download the csv
        $headers = [
                'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=loans.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];
        return Response::stream($callback, 200, $headers);
    }

        public function loansPDF() {
    //Fetch all loans from database
    $loans = Loan::all()->toArray();
        //dd($loans);
    // // Send data to the view using loadView function of PDF facade
    $pdf = PDF::loadView('pdf.loans', compact('loans'));

    // Finally, you can download the file using download function

    return $pdf->stream();
  }

        public function summariesPDF() {
        //Receive search results from user

        //Get all loans that match the idno
        // Send data to the view using loadView function of PDF facade
        $pdf = PDF::loadView('summariesPDF');
        // If you want to store the generated pdf to the server then you can use the store function
        // $pdf->save(storage_path().'_filename.pdf');
        // Finally, you can download the file using download function
        return $pdf->download('summaries.pdf');
    }

    public function largeCSV(){
        //////////////////////////////////////

        $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');
            fputcsv($handle, array('Loan serial No.','Student Name', 'ID No.', 'Admission No.', 'Product','Interest rate', 'Academic Yr.', 'Institution Code', 'Institution Name', 'Start Yr.', 'End Yr.','Mature', 'Maturity Date','Principal', 'Principal repaid', 'Principal Balance', 'interest charged', 'interest repaid', 'interest balance', 'ledger fee charged', 'ledger fee repaid', 'ledger Fee balance', 'insurance charged', 'insurance repaid', 'insurance balance', 'penalty charged', 'penalty repaid', 'penalty balance','waivers','Total Paid','Running Bal','last Pay Date'));
             // Get all users
            Loan::where('institution_code','!=','NON')->chunk(5000, function($loans) use($handle){
            foreach ($loans as $loan) {
             // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->loan_category_code,
                    $loan->interest_rate,
                    $loan->academic_year,
                    $loan->institution_code,
                    $loan->university,
                    $loan->start_year,
                    //$loan->loan_maturity_date,                    
                    date('Y', strtotime($loan->loan_maturity_date)),
                    $loan->loan_maturity_date<=Carbon::today()?'Mature':'Unmature',
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    $loan->principal_repaid,
                    $loan->principal_disbursed-$loan->principal_repaid,
                    $loan->interest_charged,
                    $loan->interest_repaid,
                    $loan->interest_charged-$loan->interest_repaid,
                    $loan->ledger_charged,
                    $loan->ledger_repaid,
                    $loan->ledger_charged-$loan->ledger_repaid,
                    $loan->insurance_charged,
                    $loan->insurance_repaid,
                    $loan->insurance_charged-$loan->insurance_repaid,
                    $loan->penalty_charged,
                    $loan->penalty_repaid,
                    $loan->penalty_charged-($loan->penalty_repaid*-1),
                    $loan->waivers_amount,
                    $loan->principal_repaid+$loan->interest_repaid+$loan->ledger_repaid+$loan->insurance_repaid+$loan->penalty_repaid,
                    $loan->outstanding_balance,
                    $loan->lasttrans_date,

                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="Loanbook.csv"',
            ]);

        return $response->send();
        /////////////////////////////////////////////////
    }


    public function loansperyearCSV(){
        //////////////////////////////////////

        $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');
            fputcsv($handle, array('Loan serial No.','Student Name', 'ID No.', 'Admission No.', 'Product','Interest rate', 'Academic Yr.', 'Institution Code', 'Institution Name', 'Start Yr.', 'End Yr.','Mature', 'Maturity Date','Principal','Principal Opening Balance', 'Principal repaid', 'Principal Balance','Interest Opening Balance', 'interest charged', 'interest repaid', 'interest balance','Ledger Opening Balance', 'ledger fee charged', 'ledger fee repaid', 'ledger Fee balance','Insurance Opening Balance', 'insurance charged', 'insurance repaid', 'insurance balance','Penalty Opening Balance', 'penalty charged', 'penalty repaid', 'penalty balance','waivers','Total Paid','Running Bal','last Pay Date'));
             // Get all users
            Loansperyear::where('institution_code','!=','NON')->where('lmperiod','=','20172018')->chunk(5000, function($loans) use($handle){
           
            foreach ($loans as $loan) {
             // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->loan_category_code,
                    $loan->interest_rate,
                    $loan->academic_year,
                    $loan->institution_code,
                    $loan->university,
                    $loan->start_year,
                    //$loan->loan_maturity_date,                    
                    date('Y', strtotime($loan->loan_maturity_date)),
                    $loan->is_matured>0?'Mature':'Unmature',
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    $loan->principal_opening_balance,
                    $loan->principal_repaid,
                    $loan->principal_opening_balance -$loan->principal_repaid,
                    $loan->interest_opening_balance,                   
                    $loan->interest_charged,
                    $loan->interest_repaid,
                    $loan->interest_opening_balance + $loan->interest_charged-$loan->interest_repaid,
                    $loan->ledger_opening_balance,
                    $loan->ledger_charged,
                    $loan->ledger_repaid,
                    $loan->ledger_opening_balance+$loan->ledger_charged-$loan->ledger_repaid,
                    $loan->insurance_opening_balance,
                    $loan->insurance_charged,
                    $loan->insurance_repaid,
                    $loan->insurance_opening_balance+$loan->insurance_charged-$loan->insurance_repaid,
                    $loan->penalty_opening_balance,
                    $loan->penalty_charged,
                    $loan->penalty_repaid,
                    $loan->penalty_opening_balance+$loan->penalty_charged-($loan->penalty_repaid*-1),
                    $loan->waivers_amount,
                    $loan->principal_repaid+$loan->interest_repaid+$loan->ledger_repaid+$loan->insurance_repaid+$loan->penalty_repaid,
                    $loan->outstanding_balance,
                    $loan->lasttrans_date,

                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="Loanbook_1718.csv"',
            ]);

        return $response->send();
        /////////////////////////////////////////////////
    }


    public function loansperquarterCSV(){
        //////////////////////////////////////

        $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');
            fputcsv($handle, array('Loan serial No.','Student Name', 'ID No.', 'Admission No.', 'Product','Interest rate', 'Academic Yr.', 'Institution Code', 'Institution Name', 'Start Yr.', 'End Yr.','Mature', 'Maturity Date','Principal','Principal Opening Balance', 'Principal repaid', 'Principal Balance','Interest Opening Balance', 'interest charged', 'interest repaid', 'interest balance','Ledger Opening Balance', 'ledger fee charged', 'ledger fee repaid', 'ledger Fee balance','Insurance Opening Balance', 'insurance charged', 'insurance repaid', 'insurance balance','Penalty Opening Balance', 'penalty charged', 'penalty repaid', 'penalty balance','waivers','Total Paid','Running Bal','last Pay Date'));
             // Get all users
            Loansperquarter::where('institution_code','!=','NON')->where('lmperiod','=','1819_1')->chunk(5000, function($loans) use($handle){
           
            foreach ($loans as $loan) {
             // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->loan_category_code,
                    $loan->interest_rate,
                    $loan->academic_year,
                    $loan->institution_code,
                    $loan->university,
                    $loan->start_year,
                    //$loan->loan_maturity_date,                    
                    date('Y', strtotime($loan->loan_maturity_date)),
                    $loan->is_matured>0?'Mature':'Unmature',
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    $loan->principal_opening_balance,
                    $loan->principal_repaid,
                    $loan->principal_opening_balance -$loan->principal_repaid,
                    $loan->interest_opening_balance,                   
                    $loan->interest_charged,
                    $loan->interest_repaid,
                    $loan->interest_opening_balance + $loan->interest_charged-$loan->interest_repaid,
                    $loan->ledger_opening_balance,
                    $loan->ledger_charged,
                    $loan->ledger_repaid,
                    $loan->ledger_opening_balance+$loan->ledger_charged-$loan->ledger_repaid,
                    $loan->insurance_opening_balance,
                    $loan->insurance_charged,
                    $loan->insurance_repaid,
                    $loan->insurance_opening_balance+$loan->insurance_charged-$loan->insurance_repaid,
                    $loan->penalty_opening_balance,
                    $loan->penalty_charged,
                    $loan->penalty_repaid,
                    $loan->penalty_opening_balance+$loan->penalty_charged-($loan->penalty_repaid*-1),
                    $loan->waivers_amount,
                    $loan->principal_repaid+$loan->interest_repaid+$loan->ledger_repaid+$loan->insurance_repaid+$loan->penalty_repaid,
                    $loan->outstanding_balance,
                    $loan->lasttrans_date,

                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="Loanbook_1stQ_1819.csv"',
            ]);

        return $response->send();
        /////////////////////////////////////////////////
    }


    public function defaultersCSV(){
        //////////////////////////////////////
        $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');
            fputcsv($handle, array('SNo.','Student Name','phone','email', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'university', 'Start Yr.', 'End Yr.', 'Maturity Date','Principal', 'Running Bal','Last pay date'));
           
            Loan::defaulted()->chunk(5000, function($loans) use($handle){
            foreach ($loans as $loan) {
                // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->phone_num,
                    $loan->email,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->institution_code,
                    $loan->loan_category_code,
                    $loan->academic_year,
                    $loan->university,
                    $loan->start_year,
                     date('Y', strtotime($loan->loan_maturity_date)),
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    $loan->running_balance,
                    $loan->lasttrans_date 
                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="defaultersList.csv"',
            ]);

        return $response->send();
        /////////////////////////////////////////////////
    }
    public function unmatureCSV(){
        //////////////////////////////////////
        $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');
            fputcsv($handle, array('SNo.','Student Name','phone','email', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'university', 'Start Yr.', 'End Yr.', 'Maturity Date','Principal', 'Running Bal','Last pay date'));
             Loan::unmatured()->chunk(5000, function($loans) use($handle){
            foreach ($loans as $loan) {
                // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->phone_num,
                    $loan->email,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->institution_code,
                    $loan->loan_category_code,
                    $loan->academic_year,
                    $loan->university,
                    $loan->start_year,
                     date('Y', strtotime($loan->loan_maturity_date)),
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    $loan->running_balance,
                    $loan->lasttrans_date 
                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="defaultersList.csv"',
            ]);

        return $response->send();
        /////////////////////////////////////////////////
    }


      

        public function dormantLoansCSV(){
        //////////////////////////////////////
        $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');

            // Add CSV headers

            // 'SNo.', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'Principal', 'Principal Paid', 'Principal C/F', 'Int. Charged', 'Int. Paid', 'Int. C/F', 'Ldg. Charged', 'Ldg. Paid', 'Ldg. C/F', 'Ins. Charged', 'Ins Paid', 'Ins. C/F', 'Penalty Charged', 'Penalty Paid', 'Penalty C/F'

            fputcsv($handle, array('SNo.','Student Name','phone','email', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'university', 'Start Yr.', 'End Yr.', 'Maturity Date','Principal','Running Bal','Last pay date'));
            //$loans = Loan::has('statements')->skip(0)->limit(500)->get();
            // Get all users

            Loan::dormantloans()->chunk(5000, function($loans) use($handle){
            foreach ($loans as $loan) {
                // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->phone_num,
                    $loan->email,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->institution_code,
                    $loan->loan_category_code,
                    $loan->academic_year,
                    $loan->university,
                    $loan->start_year,
                     date('Y', strtotime($loan->loan_maturity_date)),
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    // $loan->principal_repaid,
                    // $loan->interest_charged,
                    // $loan->interest_repaid,
                    // $loan->ledger_charged,
                    // $loan->ledger_repaid,
                    // $loan->insurance_charged,
                    // $loan->insurance_repaid,
                    // $loan->penalty_charged,
                    // $loan->penalty_repaid,
                    // $loan->waivers_amount,
                    $loan->running_balance,
                    $loan->lasttrans_date 
                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="dormantLoansList.csv"',
            ]);

        return $response->send();
        /////////////////////////////////////////////////
    }

    public function clearanceProjectionCSV(){
        $noofmonths = 2;
       $amount =3000*$noofmonths;
        $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');

               fputcsv($handle, array('SNo.','Student Name','phone','email', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'university', 'Start Yr.', 'End Yr.', 'Maturity Date','Principal', 'Principal repaid', 'interest charged', 'interest repaid', 'ledger charged', 'ledger repaid', 'insurance charged', 'insurance repaid', 'penalty charged', 'penalty repaid','waivers','Running Bal'));

            // $noofmonths = request('noofmonths');

     // $results = Loan::whereBetween('outstanding_balance', array(1, $amount))->get();


            Loan::whereBetween('outstanding_balance', array(1, $amount))->chunk(5000, function($loans) use($handle){
            foreach ($loans as $loan) {
                // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->phone_num,
                    $loan->email,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->institution_code,
                    $loan->loan_category_code,
                    $loan->academic_year,
                    $loan->university,
                    $loan->start_year,
                    date('Y', strtotime($loan->loan_maturity_date)),
                   // $loan->end_year,
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    $loan->principal_repaid,
                    $loan->interest_charged,
                    $loan->interest_repaid,
                    $loan->ledger_charged,
                    $loan->ledger_repaid,
                    $loan->insurance_charged,
                    $loan->insurance_repaid,
                    $loan->penalty_charged,
                    $loan->penalty_repaid,
                    $loan->waivers_amount,
                    $loan->running_balance
                    //$loan->outstanding_balance
                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="clearanceprojection.csv"',
            ]);

        return $response->send();
        /////////////////////////////////////////////////
    }

public function dormantfilter(Request $request){
    $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');
            fputcsv($handle, array('SNo.','Student Name','phone','email', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'university', 'Start Yr.', 'End Yr.', 'Maturity Date','Principal', 'Running Bal','Last pay date'));
           
            Loan::where('loan_category_code', request('product'))->matured()->npl()->dormantloans()->chunk(5000, function($loans) use($handle){
            foreach ($loans as $loan) {
                // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->phone_num,
                    $loan->email,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->institution_code,
                    $loan->loan_category_code,
                    $loan->academic_year,
                    $loan->university,
                    $loan->start_year,
                     date('Y', strtotime($loan->loan_maturity_date)),
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    $loan->running_balance,
                    $loan->lasttrans_date 
                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="defaultersList.csv"',
            ]);

        return $response->send();
    }


public function unmaturedfilter(Request $request){
    $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');
            fputcsv($handle, array('SNo.','Student Name','phone','email', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'university', 'Start Yr.', 'End Yr.', 'Maturity Date','Principal', 'Running Bal'));
           
            Loan::where('loan_category_code', request('product'))->unmatured()->chunk(5000, function($loans) use($handle){
            foreach ($loans as $loan) {
                // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->phone_num,
                    $loan->email,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->institution_code,
                    $loan->loan_category_code,
                    $loan->academic_year,
                    $loan->university,
                    $loan->start_year,
                     date('Y', strtotime($loan->loan_maturity_date)),
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    $loan->running_balance
                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="Unmatured List.csv"',
            ]);

        return $response->send();
    }


    public function loansfiltered(Request $request){
        //////////////////////////////////////

        $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');
            fputcsv($handle, array('Loan serial No.','Student Name', 'ID No.', 'Admission No.', 'Product','Interest rate', 'Academic Yr.', 'Institution Code', 'Institution Name', 'Start Yr.', 'End Yr.','Mature', 'Maturity Date','Principal', 'Principal repaid', 'Principal Balance', 'interest charged', 'interest repaid', 'interest balance', 'ledger fee charged', 'ledger fee repaid', 'ledger Fee balance', 'insurance charged', 'insurance repaid', 'insurance balance', 'penalty charged', 'penalty repaid', 'penalty balance','waivers','Total Paid','Running Bal','last Pay Date'));
             // Get all users
            Loan::where('loan_category_code', request('product'))->where('institution_code','!=','NON')->chunk(5000, function($loans) use($handle){
            foreach ($loans as $loan) {
             // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->loan_category_code,
                    $loan->interest_rate,
                    $loan->academic_year,
                    $loan->institution_code,
                    $loan->university,
                    $loan->start_year,
                    //$loan->loan_maturity_date,                    
                    date('Y', strtotime($loan->loan_maturity_date)),
                    $loan->is_matured>0?'Mature':'Unmature',
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    $loan->principal_repaid,
                    $loan->principal_disbursed-$loan->principal_repaid,
                    $loan->interest_charged,
                    $loan->interest_repaid,
                    $loan->interest_charged-$loan->interest_repaid,
                    $loan->ledger_charged,
                    $loan->ledger_repaid,
                    $loan->ledger_charged-$loan->ledger_repaid,
                    $loan->insurance_charged,
                    $loan->insurance_repaid,
                    $loan->insurance_charged-$loan->insurance_repaid,
                    $loan->penalty_charged,
                    $loan->penalty_repaid,
                    $loan->penalty_charged-($loan->penalty_repaid*-1),
                    $loan->waivers_amount,
                    $loan->principal_repaid+$loan->interest_repaid+$loan->ledger_repaid+$loan->insurance_repaid+$loan->penalty_repaid,
                    $loan->outstanding_balance,
                    $loan->lasttrans_date,

                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="Loanbook.csv"',
            ]);

        return $response->send();
        /////////////////////////////////////////////////
    }


       public function clearedloansfiltered(Request $request){
            $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');
            
            fputcsv($handle, array('SNo.','Student Name','phone','email', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'university', 'Start Yr.', 'End Yr.', 'Maturity Date','Principal', 'Principal repaid', 'interest charged', 'interest repaid', 'ledger charged', 'ledger repaid', 'insurance charged', 'insurance repaid', 'penalty charged', 'penalty repaid','waivers','Running Bal'));
            //$loans = Loan::has('statements')->skip(0)->limit(500)->get();
            // Get all users

            Loan::where('loan_category_code', request('product'))->cleared()->chunk(5000, function($loans) use($handle){
            foreach ($loans as $loan) {
                // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->phone_num,
                    $loan->email,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->institution_code,
                    $loan->loan_category_code,
                    $loan->academic_year,
                    $loan->university,
                    $loan->start_year,
                   // $loan->end_year,
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    $loan->principal_repaid,
                    $loan->interest_charged,
                    $loan->interest_repaid,
                    $loan->ledger_charged,
                    $loan->ledger_repaid,
                    $loan->insurance_charged,
                    $loan->insurance_repaid,
                    $loan->penalty_charged,
                    $loan->penalty_repaid,
                    $loan->waivers_amount,
                    //$loan->running_balance
                    $loan->outstanding_balance
                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="clearedList.csv"',
            ]);

        return $response->send();
    }

public function clearedLoansCsv(){
            $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');

            // Add CSV headers

            // 'SNo.', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'Principal', 'Principal Paid', 'Principal C/F', 'Int. Charged', 'Int. Paid', 'Int. C/F', 'Ldg. Charged', 'Ldg. Paid', 'Ldg. C/F', 'Ins. Charged', 'Ins Paid', 'Ins. C/F', 'Penalty Charged', 'Penalty Paid', 'Penalty C/F'

            fputcsv($handle, array('SNo.','Student Name','phone','email', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'university', 'Start Yr.', 'End Yr.', 'Maturity Date','Principal', 'Principal repaid', 'interest charged', 'interest repaid', 'ledger charged', 'ledger repaid', 'insurance charged', 'insurance repaid', 'penalty charged', 'penalty repaid','waivers','Running Bal'));
            //$loans = Loan::has('statements')->skip(0)->limit(500)->get();
            // Get all users

            Loan::cleared()->chunk(5000, function($loans) use($handle){
            foreach ($loans as $loan) {
                // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->phone_num,
                    $loan->email,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->institution_code,
                    $loan->loan_category_code,
                    $loan->academic_year,
                    $loan->university,
                    $loan->start_year,
                   // $loan->end_year,
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    $loan->principal_repaid,
                    $loan->interest_charged,
                    $loan->interest_repaid,
                    $loan->ledger_charged,
                    $loan->ledger_repaid,
                    $loan->insurance_charged,
                    $loan->insurance_repaid,
                    $loan->penalty_charged,
                    $loan->penalty_repaid,
                    $loan->waivers_amount,
                    //$loan->running_balance
                    $loan->outstanding_balance
                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="clearedList.csv"',
            ]);

        return $response->send();
    }




public function maturedfiltered(Request $request){
    $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');
            fputcsv($handle, array('SNo.','Student Name','phone','email', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'university', 'Start Yr.', 'End Yr.', 'Maturity Date','Principal', 'Running Bal','Last pay date'));
           
            Loan::where('loan_category_code', request('product'))->matured()->chunk(5000, function($loans) use($handle){
            foreach ($loans as $loan) {
                // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->phone_num,
                    $loan->email,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->institution_code,
                    $loan->loan_category_code,
                    $loan->academic_year,
                    $loan->university,
                    $loan->start_year,
                     date('Y', strtotime($loan->loan_maturity_date)),
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    $loan->running_balance,
                    $loan->lasttrans_date

                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="Unmatured List.csv"',
            ]);

        return $response->send();
    }



public function maturedfilter(){
    $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');
            fputcsv($handle, array('SNo.','Student Name','phone','email', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'university', 'Start Yr.', 'End Yr.', 'Maturity Date','Principal', 'Running Bal','Last pay date'));
           
            Loan::matured()->chunk(5000, function($loans) use($handle){
            foreach ($loans as $loan) {
                // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->phone_num,
                    $loan->email,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->institution_code,
                    $loan->loan_category_code,
                    $loan->academic_year,
                    $loan->university,
                    $loan->start_year,
                     date('Y', strtotime($loan->loan_maturity_date)),
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    $loan->running_balance,
                    $loan->lasttrans_date

                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="matured List.csv"',
            ]);

        return $response->send();
    }


    public function calculateLoanFields(){
        //$loans = Loan::has('statements')->where('is_calculated', '=', 0)->skip(0)->limit(100)->get();

        Loan::has('statements')->where('is_calculated', '=', 0)->chunk(5000, function($loans){
            foreach ($loans as $key => $loan) {
                $loan->principal_repaid = $loan->statements()->principalrepayment()->sum('credit_amount');
                $loan->save();
                echo $loan->id;
            }
        });
    }

    public function calculateInterestCharged(){
        //$loans = Loan::has('statements')->where('is_calculated', '=', 0)->skip(0)->limit(100)->get();

        Loan::has('statements')->where('is_calculated', '=', 0)->chunk(100, function($loans){
            foreach ($loans as $key => $loan) {
                $loan->interest_charged = $loan->statements()->interestcharged()->sum('debit_amount');
                $loan->save();
                Log::info('Interest calculated for '.$loan->id);
            }
        });
    }
}
