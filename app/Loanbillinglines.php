<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loanbillinglines extends Model
{
    
     public function createLines($arr,$header_id) {
     	//dd($arr->lines);
     if($arr){
        foreach ($arr->lines as $key => $line) {
      $last = Loancheckofflines::where('nationalidno',$line->NATIONALIDNO)->orderBy('documentdate', 'DESC')->first();
      $det = Loan::where('id_no',$line->NATIONALIDNO)->first();
     
      	$lastpaydate = isset($last->documentdate) ? $last->documentdate:'1900-01-01';
    		$billhonored=$lastpaydate>$arr->datedue?1:0;
        	
         $record = [
                            "loanbillingheaders_id"=>$header_id,
                            "name"=>$det->username,
                            "phone"=>$det->phone_num,
                            "email"=>$det->email,
                            "accountnum"=>$line->ACCOUNTNUM,
                            "idno" => $line->NATIONALIDNO,
                            "lastpaydate" => $line->LASTPAYMENTDATE,
                            "loanproductcode" => $line->LOANPRODUCTCODE,
                            "loanserialno" => $line->LOANSERIALNO,
                            "outstanding_balance" => number_format($line->OUTSTANDINGBALANCE, 2, '.', ''),
                            "repaymentrate" => number_format($line->RATEOFPAYMENT, 2, '.', ''),
                            "bill_honored" => $billhonored,
                            "lastpaydateafterbill" => $lastpaydate
                        ];
                        
                        //$data[] = $record;
                        Loanbillinglines::insert($record);
      }
      return ;

      }
     
    }

    public function billingheader(){
    	return $this->belongsTo('App\Loanbillingheader');
    }

}
