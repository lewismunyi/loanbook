<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loancheckoffheader extends Model
{
     public function createHeader($arr) {
     	//dd($arr);
     	 $record = [
                            "recid"=>$arr->RECID,
                            "checkoffbatchno" => $arr->CHECKOFFBATCHNO,
                            "amount" => number_format($arr->AMOUNT, 2, '.', ''),
                            "checkoffdate" => $arr->CHECKOFFDATE,
                            "checkoffperiod" => $arr->CHECKOFFPERIOD,
                    		"custpaymreference"=>$arr->CUSTPAYMREFERENCE,
                            "dateentered" => $arr->DATEENTERED,
                            "documentno" => $arr->DOCUMENTNO,
                            "documentdate" => $arr->DOCUMENTDATE,
                            "employercode" => $arr->EMPLOYERCODE,
                            "posted" => $arr->POSTED,
                            "postedby"=>$arr->POSTEDBY,
                            "dateposted" => $arr->DATEPOSTED,
                            "paymmode" => $arr->PAYMMODE,
                            "postedjournalnum" => $arr->POSTEDJOURNALNUM,
                            "penaltyamount" => number_format($arr->PENALTYAMOUNT, 2, '.', ''),
                     		"reversed" => $arr->REVERSED,
                        ];
                      

       return $id= $this->insertGetId($record); 
    }

}
