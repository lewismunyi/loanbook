<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Loanbillingheader extends Model
{
    
    
     public function createHeader($arr) {
     // dd($arr);

         $record = [
                            "batchno"=>$arr->batchno,
                            "officer" => $arr->officer,
                            "empcode" => $arr->empcode,
                            "datecreated" => $arr->datecreated,
                            "datedue" => $arr->datedue,
                            "recid" => $arr->RECID
                        ];
                      

       return $id= $this->insertGetId($record); 
    }
 public function getreportdata($startDate,$endDate,$empcode,$officer) {
    
         $res  = Loanbillingheader::whereBetween('datecreated', array($startDate, $endDate))->with('billingLines')->get();
         if(isset($empcode) && !empty($empcode)  && (!isset($officer) || empty($officer))){
            $res  = Loanbillingheader::whereBetween('datecreated', array($startDate, $endDate))
                                        ->whereEmpcode($empcode)->with('billingLines')->get();
        }
         if(isset($officer) && !empty($officer) && (!isset($empcode) || empty($empcode))){
            $res  = Loanbillingheader::whereBetween('datecreated', array($startDate, $endDate))
                                        ->whereOfficer($officer)->with('billingLines')->get();
        }
        if(isset($officer) && !empty($officer) && (isset($empcode) || !empty($empcode))){
            $res  = Loanbillingheader::whereBetween('datecreated', array($startDate, $endDate))
                                        ->whereOfficer($officer)->whereEmpcode($empcode)->with('billingLines')->get();
        }
          return $res;
         // $users = User::with('comments')->get();  
    }

        public function billingLines(){
    	return $this->hasMany('App\Loanbillinglines','loanbillingheaders_id');
    }

     public function billingLinesnothonored()
{
 
     return $this->hasMany('App\Loanbillinglines','loanbillingheaders_id')
                    ->select(DB::raw('idno,lastpaydate,outstanding_balance'))
                    ->where('bill_honored', 0);
}

 public function billingLineshonored()
{
 
     return $this->hasMany('App\Loanbillinglines','loanbillingheaders_id')
                    ->select(DB::raw('idno,lastpaydate,outstanding_balance'))
                    ->where('bill_honored', 1);
}



}
