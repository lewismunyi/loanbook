<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanStatement extends Model
{
	//protected $table = 'loan_statements_product';
	protected $fillable = [
		'loan_id',
		'serial_num',
		'rec_id',
		'debit_amount',
		'credit_amount',
		'transaction_date',
		'transaction_period',
		'transaction_type',
		'transaction_description'
	];
    public function loan(){
    	return $this->belongsTo('App\Loan');
    }

    public function scopePrincipalrepayment($query){
    	return $query->where('transaction_type', '=', 4);
    }

    public function scopeInterestcharged($query){
    	return $query->where('transaction_type', '=', 5);
    }

    public function scopeInterestpaid($query){
    	return $query->where('transaction_type', '=', 6);
    }

    public function scopePenaltycharged($query){
        return $query->where('transaction_type', '=', 7);
    }

    public function scopePenaltypaid($query){
        return $query->where('transaction_type', '=', 8);
    }

    public function scopeLegerfeecharged($query){
    	return $query->where('transaction_type', '=', 9);
    }

    public function scopeLegerfeepaid($query){
        return $query->where('transaction_type', '=', 10);
    }
 	public function scopeTotalamountpaid($query){
        //return $query->where('transaction_type', '=', 10);
        return $query->whereIn('transaction_type', array(10, 6, 4));
    }

    public function scopeInsurancecharged($query){
        return $query->where('transaction_type', '=', 13);
    }

    public function scopeInsurancepaid($query){
        return $query->where('transaction_type', '=', 14);
    }
        //getrunning balance from ax
    public function doAPIcall($arr,$action) {

        //$url = "http://197.136.52.3:1924/esb.php?rquest=$action";
       // $url = "http://192.168.1.96:1924/esb.php?rquest=$action";
        $url = "http://localhost/dome/esb.php?rquest=$action";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($arr));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);
        //dd($response);
        return $response;
       

    }


}
