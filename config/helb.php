<?php
return [
	'institution_types' => [
		['name'=>'public-universities', 'title' => 'Public Universities'],
		['name'=>'private-universities', 'title' => 'Private Universities'],
		['name'=>'tvet', 'title' => 'Technical Institutions']
	]
]
?>