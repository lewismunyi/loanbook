@extends('layouts.auth')

@section('content')


<div class="login-body">
<!-- <a class="login-brand" href="index-2.html">
  <img class="img-responsive" src="" alt="Elephant">
</a> -->
<div class="login-form">
  <form data-toggle="validator" method="POST" action="{{ route('password.email') }}">
  {{ csrf_field() }}
    <div class="form-group">
      <label for="email">Email</label>
      <input id="email" class="form-control" type="email" name="email" spellcheck="false" autocomplete="off" data-msg-required="Please enter your email address." required value="{{ old('email') }}">
      <p class="help-block">
        <small>If you've forgotten your password, we'll send you an email to reset your password.</small>
      </p>
    </div>
    
    
    <button class="btn btn-primary btn-block" type="submit">Send Password Reset Link</button>
  </form>
</div>
</div>
<div class="login-footer">
Don't have an account? <a href="{{ route('login') }}">Login</a>
</div>

@endsection
