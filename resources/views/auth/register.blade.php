@extends('layouts.auth')

@section('content')

<div class="login-body">
<!-- <a class="login-brand" href="index-2.html">
  <img class="img-responsive" src="" alt="Elephant">
</a> -->
<div class="login-form">
  <form data-toggle="validator" class="validated" method="POST" action="{{ route('register') }}">
  {{ csrf_field() }}
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" id="name" class="form-control" name="name" spellcheck="false" data-msg-required="Please enter your name." required="" aria-required="true" value="{{ old('name') }}">
    </div>

    <div class="form-group">
      <label for="email">Email</label>
      <input id="email" class="form-control" type="email" name="email" spellcheck="false" autocomplete="off" data-msg-required="Please enter your email address." required>
    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input id="password" class="form-control" type="password" name="password" minlength="6" data-msg-minlength="Password must be 6 characters or more." data-msg-required="Please enter your password." required>
    </div>

    <div class="form-group">
      <label for="password_confirmation">Confirm Password</label>
      <input id="password_confirmation" class="form-control" type="password" name="password_confirmation" minlength="6" data-msg-minlength="Password must be 6 characters or more." data-msg-required="Please enter your password." required>
    </div>

    <div class="form-group">
      <label class="custom-control custom-control-primary custom-checkbox">
        <input class="custom-control-input" type="checkbox" name="tncs" required>
        <span class="custom-control-indicator"></span>
        <span class="custom-control-label">I agree to terms and conditions</span>
      </label>

    </div>
    <button class="btn btn-primary btn-block" type="submit">Sign up</button>
  </form>
</div>
</div>
<div class="login-footer">
Have an account? <a href="{{ route('login') }}">Sign In</a>
</div>


@endsection
