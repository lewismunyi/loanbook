@extends('layouts.auth')

@section('content')


  <p class="text-center">Welcome back to <strong>HELB</strong>.</p>
  <form data-toggle="validator" class="validated" method="POST" action="{{ route('login') }}">
  {{ csrf_field() }}
    <div class="form-group">
      <label for="email">Email</label>
      <input id="email" class="form-control" type="email" name="email" spellcheck="false" autocomplete="off" data-msg-required="Please enter your email address." required>
    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input id="password" class="form-control" type="password" name="password" minlength="6" data-msg-minlength="Password must be 6 characters or more." data-msg-required="Please enter your password." required>
    </div>
    <div class="form-group">
      <label class="custom-control custom-control-primary custom-checkbox">
        <input class="custom-control-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
        <span class="custom-control-indicator"></span>
        <span class="custom-control-label">Keep me signed in</span>
      </label>
      {{--<span aria-hidden="true"></span>--}}

    </div>
    <button class="btn btn-primary btn-block" type="submit">Sign in</button>
  </form>
  <br>
<div class="login-footer">
Don't have an account? <a href="{{ route('register') }}">Sign Up</a><a class="float-right" href="{{ route('password.request') }}">Forgot password?</a>
</div>

@endsection
