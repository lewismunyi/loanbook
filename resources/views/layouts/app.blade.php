<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags--}}
      {{--Manifest--}}
      <link rel="manifest" href="{{asset('manifest.json')}}">
      {{--Favicons--}}
      <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/favicons/apple-touch-icon.png"')}}">
      <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/favicons/favicon-32x32.png')}}">
      <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/favicons/favicon-16x16.png')}}">
      <link rel="mask-icon" href="{{asset('img/favicons/safari-pinned-tab.svg')}}" color="#253449">
      <link rel="shortcut icon" href="{{asset('img/favicons/favicon.ico')}}">
      <link rel="favicon" href="{{asset('img/favicons/favicon.ico')}}">
      <meta name="msapplication-TileColor" content="#2b5797">
      <meta name="msapplication-config" content="{{asset('img/favicons/browserconfig.xml')}}">
      <meta name="theme-color" content="#ffffff">
      <meta name="description" content="The HELB Loanbook reports Portal">
    <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800" rel="stylesheet"> 
    <!-- Styles -->
     <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>

  <body>
  <noscript>
      <div class="alert alert-danger">
          Oops!
          It looks like javascript is disabled on your website.
          This page requires javascript to run.
      </div>
  </noscript>
  <div class="loader preloader-wrapper small active" id="preloader">
      <div class="spinner-layer spinner-green-only">
          <div class="circle-clipper left">
              <div class="circle"></div>
          </div><div class="gap-patch">
              <div class="circle"></div>
          </div><div class="circle-clipper right">
              <div class="circle"></div>
          </div>
      </div>
  </div>
  <div id="body" style="display: none">
    <!-- start the sidebar -->
    <div id="sidebar">
    <div class="sidebar-logo text-center">
        <!-- <a class="navbar-brand" href="">HELB</a> -->
        <img src="{{asset('img/logo.png')}}" width="100" alt="Logo">
        <p class="portal-name">REPORTS PORTAL</p>
      </div>
    <div id="sidebar-menu">
        @include('includes.leftsidebar')
    </div>
      
    </div>
    <!-- end the sidebar -->

    <div id="main-content">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark ">
            @yield('nav-left')
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ml-auto">
                    @yield('nav-search')
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Settings</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log out</a>
                    </div>
                    <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</button>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                </div>
            </div>
        </nav>
      <div class="page-content">
          <div class="container-fluid">
              @yield('content')
          </div>
      </div>
    </div>
  </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/d3.min.js')}}"></script>
    <script src="{{asset('js/c3.min.js')}}"></script>
    @yield('page-scripts')
  </body>
</html>