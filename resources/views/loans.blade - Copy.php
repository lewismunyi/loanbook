@extends('layouts.app')
@section('nav-left')
	<a class="navbar-brand" href="#"><h3>Loans</h3></a>
@endsection
@section('nav-search')
	<form method="POST"  action="{{route('loans-filter')}}" enctype="multipart/form-data" class="statements-search form-inline my-2 my-lg-0" id="searchCollectionReport" autocomplete="off">
        {{ csrf_field() }}
		<div class="row">
			<div class="col-sm-6">
				<input type="text" class="form-control" name="datefilter" value="" placeholder="Select date range" id="daterange" required/>
			</div>
			<div class="col-sm-6">
				<select class="custom-select mr-3" id="loanproduct" name="loanproduct" required>
					<option value="">Loan Product</option>
					<option >UG</option>
					<option >PI</option>
					<option >CI</option>
					<option >TVET</option>
					<option >AEF</option>
				</select>
			</div>
		</div>
        <button class="btn btn-outline-light my-2 ml-2 mr-5" type="submit">Get Loans</button>
    </form>
@endsection

@section('title', 'Loans')
@section('content')
	<div class="row mt-3 mb-3">
		<div class="col col-sm-12 col-lg-2 offset-5 pull-right">
			<!-- Example split danger button -->
			<div class="btn-group">
				<button type="button" class="btn btn-success btn-lg">Download </button>
				<button type="button" class="btn btn-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="sr-only">Toggle Dropdown</span>
				</button>
				<div class="dropdown-menu">
					<a href="{{route('loans-csv')}}" class="btn btn-success btn-block btn-export dropdown-item">Export CSV</a>
					<div class="dropdown-divider"></div>
					<a href="{{route('loans-pdf')}}" target="_blank" class="btn btn-success btn-block btn-export dropdown-item">Export PDF</a>
				</div>
			</div>
		</div>
	</div>

<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
				<table class="table">
					<thead>
						<th>SNo.</th>
						<th>Name</th>
						<th>ID No.</th>
						<th>Admission No.</th>
						<th>University</th>
						<th>Product</th>
						<th>Academic Yr.</th>
						<th>Principal</th>
						<th>Amount Paid</th>
						<th>Running Balance</th>
					</thead>
					<tbody>
						@if(count($loans))
						@foreach($loans as $key => $loan)
						<tr>
							<td>{{$loan->loan_serial_num}}</td>
							<td>{{$loan->username}}</td>
							<td>{{$loan->id_no}}</td>
							<td>{{$loan->student_reg_num}}</td>
							<td>{{$loan->university}}</td>
							<td>{{$loan->loan_category_code}}</td>
							<td>{{$loan->academic_year}}</td>
							<td>{{number_format($loan->principal_disbursed)}}</td>
							<td>{{number_format($loan->principal_repaid)}}</td>
							<td>{{number_format($loan->outstanding_balance)}}</td>


						</tr>
						@endforeach
						@endif
					</tbody>
				</table>

				<nav>
					<ul class="pagination justify-content-center">
						{{$loans->links('vendor.pagination.bootstrap-4')}}
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>







<!-- Modal -->

@endsection

@section('page-scripts')
@endsection
