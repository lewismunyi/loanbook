@extends('layouts.app')
@section('nav-left')
	<a class="navbar-brand" href="#"><h3>Loan defaults</h3></a>
@endsection
@section('nav-search')
	
@endsection

@section('title', 'Loans')
@section('content')
	<div class="row">
		<div class="col-lg-2 col-sm-12 offset-5">
			<a class="btn btn-success btn-block btn-export text-white" href="{{route('unmatured-loans-csv')}}">Export CSV</a>
		</div>
	</div>

<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
				<table class="table">
					<thead>
						<th>SNo.</th>
						<th>Name</th>
						<th>ID No.</th>
						<th>Admission No.</th>
						<th>University</th>
						<th>Category</th>
						<th>Academic Yr.</th>
						<th>Principal Amount</th>
						<th>last pay date</th>
						<th>outstanding balance</th>
						<th>phone</th>
						<th>email</th>
					</thead>
					<tbody>
						@if(count($loans))
						@foreach($loans as $key => $loan)
						<tr>
							<td>{{$loan->loan_serial_num}}</td>
							<td>{{$loan->username}}</td>
							<td>{{$loan->id_no}}</td>
							<td>{{$loan->student_reg_num}}</td>
							<td>{{$loan->institution_code}}</td>
							<td>{{$loan->loan_category_code}}</td>
							<td>{{$loan->academic_year}}</td>
							<td>{{$loan->principal_disbursed}}</td>
							<td>{{$loan->lasttrans_date}}</td>
							<td>{{$loan->outstanding_balance}}</td>
							<td>{{$loan->phone_num}}</td>
							<td>{{$loan->email}}</td>
							
						</tr>
						@endforeach
						@endif
					</tbody>
				</table>

				<nav>
					<ul class="pagination justify-content-center">
						{{$loans->links('vendor.pagination.bootstrap-4')}}
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>
@endsection

@section('page-scripts')
@endsection
