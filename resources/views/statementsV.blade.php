@extends('layouts.app')
@section('nav-left')
	<a class="navbar-brand" href="#"><h3>Statements</h3></a>
@endsection
@section('nav-search')
    <form onsubmit="return false;" class="statements-search form-inline my-2 my-lg-0" id="searchForm" v-on:submit="getLoans">
        <input class="form-control mr-sm-2" type="search" placeholder="Enter ID number" aria-label="Search" v-model="searchTerm">
        <button class="btn btn-outline-light my-2 mr-5" type="submit">Search</button>
    </form>
@endsection
@section('title', 'Statements')
@section('content')
<div class="container-fluid" >
    {{--Student table--}}
    <br>
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div id="preloader" style="display: none;" class="lds-ring"><div></div><div></div><div></div><div></div></div>
				<div class="card-header d-flex justify-content-between">
                    <h3 class="card-title">Student Name</h3>
                    <button data-toggle="modal" data-target="#summaryModal" v-on:click="getSummary" type="button" class="btn btn-primary">Get Summary statement</button>
                </div>
				<div class="card-body">
					<div id="loanResults">
						<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Year</th>
								<th>Product</th>
								<th>Serial No.</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<tr v-for="loan in loans">
								<td>@{{loan.id}}</td>
								<td>@{{loan.year}}</td>
								<td>@{{loan.product}}</td>
								<td>@{{loan.serialno}}</td>
								<td>@{{loan.status}}</td>
							</tr>


						</tbody>
					</table>
					</div>

				</div>
			</div>
		</div>
		<!-- Datepicker -->
	</div>


	</div>


	<div id="summaryModal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">dialog">
	  <div class="modal-dialog modal-lg">
		<div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalCenterTitle">Loan Summary</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
		  <div class="modal-body" id="loansSummary">
			<table class="table">
						<thead>
							<th>Date</th>
							<th>Ref No.</th>
							<th>Description</th>
							<th>Period</th>
							<th>Debit</th>
							<th>Credit</th>
							<th>Running Amount</th>
							<th></th>
						</thead>
						<tbody>
						<tr v-for="summary in summaries">
							<td>@{{summary.date}}</td>
							<td>@{{summary.refno}}</td>
							<td>@{{summary.description}}</td>
							<td>@{{summary.period}}</td>
							<td>@{{summary.debit}}</td>
							<td>@{{summary.credit}}</td>
							<td>@{{summary.runningbalance}}</td>
							<td></td>
						</tr>
						</tbody>
					</table>
		  </div>
		  <div class="modal-footer">
              <a class="btn btn-success" href="{{route('summaries-pdf')}}">Download PDF</a>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>

</div>
@endsection

@section('page-scripts')
<script type="text/javascript">
var searchForm = new Vue({
	el: 'form#searchForm',
	data: {
		searchTerm : ''
	},
	methods: {
		getLoans: function(){
			$('#preloader').show();
			var self = this;
			//console.log('Searching...'+self.searchTerm);
			//////////////////////////////////////////////
			// Make a request for a user with a given ID
			axios.get("{{route('create-statement-lines')}}", {
			    params: {
			      idno: self.searchTerm
			    }
			  })
			  .then(function (response) {
			    console.log(response.data);
			    loanResults.loans = response.data.loans
			    $('#preloader').fadeOut();
			  })
			  .catch(function (error) {
			    console.log(error);
			  })
			  .then(function () {
			    // always executed
			  });
			/////////////////////////////////////////////////
		}
	}
});

////Loan Results
var loanResults = new Vue({
	el: '#loanResults',
	data: {
		loans: [
			{'id': 1, 'year': '2000/2001', 'product': 'UG', 'serialno': '4232324', 'status': 'Registered'},
			{'id': 2, 'year': '2005/2006', 'product': 'PG', 'serialno': '423G324', 'status': 'Disbursed'},
			{'id': 4, 'year': '2005/2006', 'product': 'PG', 'serialno': 'FR3G324', 'status': 'Disbursed'}
		]
	},
	methods: {
		getSummary: function(){
			var idno = searchForm.searchTerm;
			console.log('getting summary....'+ idno);
			// Make a request for a user with a given ID
			axios.get("{{route('get-summary')}}", {
			    params: {
			      idno: idno
			    }
			  })
			  .then(function (response) {
			    console.log(response.data.loans);
			    loansSummary.summaries = response.data.loans
			    //$('#preloader').fadeOut();
			  })
			  .catch(function (error) {
			    console.log(error);
			  })
			  .then(function () {
			    // always executed
			  });
			/////////////////////////////////////////////////
		},
		getStatements: function(){
			var idno = searchForm.searchTerm;
			console.log('getting statements....'+ idno);
		}
	}
});


/////Loan summary
var loansSummary = new Vue({
	el: '#loansSummary',
	data: {
		summaries: [
			{'date':'02-18-2017', 'refno':'REFMCDSR67', 'description':'this is a description', 'period':'Feb 2017','debit':'2200.02', 'credit':'0', 'runningbalance':'230674.70'},
			{'date':'02-18-2017', 'refno':'REFMCDSR67', 'description':'this is a description', 'period':'Feb 2017','debit':'2200.02', 'credit':'0', 'runningbalance':'230674.70'}
		]
	}
});

//Datepicker
$(document).ready(function() {
	 $('.input-daterange').datepicker({
					});
});
</script>
@endsection
