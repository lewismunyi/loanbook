@extends('layouts.app')
@section('title', 'Loanbook')
@section('content')
<div class="row">
		<div class="col-xs-12"><h4>Loanbook</h4></div>
    	<div class="col-sm-4 pull-left">
    	</div>
</div>

<section class="section">
	<div class="row">
		<div class="col-sm-4">
			<div class="card">
				<div class="card-body widget">
					<h3>{{number_format($stats->loans_count)}}</h3>
					<h4>KSHs {{number_format($stats->loans_amount)}}</h4>
					<span class="widget-label">Loans</span>
					<div class="breakdown">
						<span>Open : 1,400</span>
						<span>Closed: 23,456</span>
					</div>
					<a href="">View loanbook</a>
				</div>
			</div>
		</div>

		<div class="col-sm-4">
			<div class="card">
				<div class="card-body widget">
					<h3>{{number_format($stats->open_count)}}</h3>
					<h4>{{number_format($stats->open_amount)}}</h4>
					<span class="widget-label">Open Accounts 
<a href="#" data-toggle="tooltip" title="Hooray!">?</a>
</span>
					<div class="breakdown">
						<span>Matured : 1,400</span>
						<span>not Matured: 23,456</span>
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-4">
			<div class="card">
				<div class="card-body widget">
					<h3>{{number_format($stats->loans_count)}}</h3>
					<h4>{{number_format($stats->loans_amount)}}</h4>
					<span class="widget-label">Matured Accounts</span>
					<div class="breakdown">
						<span>PL : 1,400</span>
						<span>NPL: 23,456</span>
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-4">
			<div class="card">
				<div class="card-body widget">
					<h3>{{number_format($stats->loans_count)}}</h3>
					<h4>{{number_format($stats->loans_amount)}}</h4>
					<span class="widget-label">Paying</span>
					<div class="breakdown">
						<span>Employer : 1,400</span>
						<span>Self: 23,456</span>
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-4">
			<div class="card">
				<div class="card-body widget">
					<h3>{{number_format($stats->loans_count)}}</h3>
					<h4>{{number_format($stats->loans_amount)}}</h4>
					<span class="widget-label">Not Paying</span>
					<div class="breakdown">
						<span>Dormant : 1,400</span>
						<span>Defaults: 23,456</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section">
	<div class="row">
		<div class="col-sm-7">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Loans Summary</h3>
				</div>
				<div class="card-body">
					<div id="loanGraph"></div>
				</div>
			</div>
		</div>


		<div class="col-sm-5">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Loans Summary</h3>
				</div>
				<div class="card-body">
					<div id="loanDonut"></div>
				</div>
			</div>
		</div>

	</div>
</section>



<!-- Modal -->

@endsection

@section('page-scripts')
<script type="text/javascript">
	$(function(){
		//tooltips
		$('[data-toggle="tooltip"]').tooltip();
		///////////Loans graph
		var chart = c3.generate({
			bindto: '#loanGraph',
			data: {
		        columns: [
		            ['data1', 30, 200, 100, 400, 150, 250],
		            ['data2', 50, 20, 10, 40, 15, 25]
		        ],
		        type : 'bar'
		    }
		});
		///////////////////////////////////


		///Loan stats
		var chart = c3.generate({
			bindto: '#loanDonut',
			data: {
		        columns: [
		            ['data1', 30, 200, 100, 400, 150, 250],
		            ['data2', 50, 20, 10, 40, 15, 25]
		        ],
		        type : 'donut'
		    }
		});
		//////////////////////////////////////////

	});
</script>
@endsection
