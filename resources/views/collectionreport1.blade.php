@extends('layouts.app')
@section('nav-left')
    <a class="navbar-brand" href="#"><h3>Dashboard</h3></a>
@endsection
@section('title', 'Collection Report')
@section('content')
<div class="container-fluid">
    <br>
    {{--Update dashboard content--}}
    <div class="card">
        <div class="card-body">
                    <form method="POST" id="frmRemittance" action="{{route('home')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Product</label>
                                    <select name="academic_year" id="academic_year" class="form-control">
                                        <option value="">Select</option>
                                        <option>TVET</option>
                                        <option>CE</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Academic Year</label>
                                <select name="academic_year" id="academic_year" class="form-control">
                                    <option value="">Select</option>
                                    <option>2018/2019</option>
                                    <option>2017/2018</option>
                                </select>
                            </div>
                            <div class="form-group mt-auto">
                                <button class="btn btn-primary" id="btnValidate">Update Dashboard Content</button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
    <br>
    {{--Summaries--}}
    <section class="section">
        <div class="row">
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body widget">
                        <span class="widget-label">All Collections</span>
                        <h3>4,500,000</h3>
                        <h4></h4>

                        <div>
                            <a href="{{route('loans')}}" class="badge badge-success"> Get List</a>
                            <a href="{{route('cleared-loans')}}" class="badge badge-secondary"></a>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body widget">
                        <span class="widget-label">Interest</span>
                        <h3>45000</h3>
                        <h4></h4>

                        <div>
                            <a href="{{route('loans')}}" class="badge badge-success">8988</a>
                            <a href="" class="badge badge-danger"></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body widget">
                        <span class="widget-label">Ledger Fee</span>
                        <h3>69000</h3>
                        <h4></h4>
                        <a class="badge badge-light">-</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body widget">
                        <span class="widget-label">Insurance</span>
                        <h3>890000</h3>
                        <h4></h4>

                        <div>
                            <a href="{{route('dormant-loans')}}" class="badge badge-secondary"></a>
                            <a href="{{route('default-loans')}}" class="badge badge-danger"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

            <br>
    <section class="section">
        <div class="row">
            <div class="col-sm-7">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Chart Title</h4>
                    </div>
                    <div class="card-body">
                        <div id="loanGraph"></div>
                    </div>
                </div>
            </div>


            <div class="col-sm-5">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Chart Title</h4>
                    </div>
                    <div class="card-body">
                        <div id="loanDonut"></div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>


<!-- Modal -->

@endsection

@section('page-scripts')
<script type="text/javascript">
    $(function(){
        //tooltips
        $('[data-toggle="tooltip"]').tooltip();
        ///////////Loans graph
        var chart = c3.generate({
            bindto: '#loanGraph',
            data: {
                columns: [
                    ['Charged', 200, 100, 200,430, 150],
                    ['Paid',  20, 10, 40,50, 15]
                ],
                colors: { Charged: "#253449", Paid: "#ffc01b"},
                type : 'bar'
            },
            axis: {
                x: {
                    type: 'category', // this needed to load string x value
                    categories: [ 'Interest', 'Ledger Fee', 'Insurance','Principal', 'Penalty'],
                    //show: false
                },
                y: {
                    label: {
                        text: '',
                        position: 'outer-middle'
                    }
                }
            }
        });
        ///////////////////////////////////


        ///Loan stats
        var chart = c3.generate({
            bindto: '#loanDonut',
            data: {
                columns: [
                    ['Principal', 74],
                    ['Interest', 26]
             ['Ledger', 26]
             ['Penalty', 26]
                ],
                //colors: { PL: "#253449", NPL: "#ffc01b"},
                colors: { PL: "#253449", NPL: "#ffc01b"},
                type : 'donut'
            }
        });
        //////////////////////////////////////////

    });
</script>
@endsection
