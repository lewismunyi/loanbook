
<style type="text/css">
table {
    border-collapse: collapse;
     /*/font-family: "Trebuchet MS", sans-serif;*/
    font-size: 16px;
  
    line-height: 0.6em;
    font-style: normal;
    
}

</style>


<table width="100%">
	<tr>
		
		<td colspan="3">
			 <center><h1>HIGHER EDUCATION LOANS BOARD</h1>
	            	
	            	</center>

		</td>
	</tr>
		
<tr>
<td></td>
<td>
	<center> <img width="100px" height="100px" src="{{asset('img/logo.png')}}" class="centered" alt=""></center>
</td>			
<td style="width: 30%">
           
            <font style="font-size:9px;"> Anniversary Towers Ground Floor, University Way      <br>
            P.O Box 69489-00400, NAIROBI, KENYA  <br>
            Telephone: +254 711 052 000     <br>         
            Email: contactcentre@helb.co.ke  <br>          
            twitter.com/HELBpage <br>
            facebook.com/HELBpage
            
                </font> 

              
        

		</td>
	</tr>
	<tr>
		
		<td colspan="3">
			 <center>

	            	<h2>Loanee Statement</h2> 
	            	</center>

		</td>
	</tr>
</table>



				@if(count($headers))
					@foreach($headers  as $key => $header)
					<p>	Name:{{$header->names}}                 	ID Number:{{$header->idnumber}} 

					Accountnum:{{$header->accountnum}}
					</p>
					
					
				<p> 
				Product:{{$header->PRODUCTDESCRIPTION}}   Serial No:{{$header->loanserialno}} </p>
				
			<table  class="table table-hover" id="tblStatement" border="1">
			
				<tr class="bg-light">				
				    <td>Loan Principal</td>
				    <td>Outstanding Principal</td>
				 	<td>Outstanding insurance</td>
					<td>Outstanding interest</td>
					<td>Outstanding ledgerfee</td>
					<td>Outstanding penalty</td>
				    <td>Running Balance </td>
				
				</tr>
				<tr class="bg-secondary">				
				    <th>{{$header->loanprincipal}}</th>
				    <th>{{$header->outstanding_principal}}</th>
				 	<th>{{$header->outstanding_insurance}}</th>
					<th>{{$header->outstanding_interest}}</th>
					<th>{{$header->outstanding_ledgerfee}}</th>
					<th>{{$header->outstanding_penalty}}</th>
				    <th>{{$header->running_balance}} </th>
				
				</tr>
			
			</table>
				<p><center>Account Transactions</center></p>
			
			<table  class="table table-hover" id="tblStatement" border="1">
			
				<tbody>
					<tr>
					<th>Document No.</th>
				    <th>Transaction Date</th>
				 	<th width="35%">Description</th>
					<th width="15%">Period</th>
					<th>Charges</th>
					<th>Payments</th>
					<th>Running Balance</th>
					
				</tr>
					
					@if ($header->statementLines->count())
					      @foreach ($header->statementLines as $keyy => $line) 
					       
					      
				   <tr>
						<td>{{$line->documentnum}}</td>						
						<td>{{$line->transaction_date}}</td>
						<td>{{$line->credit_amount>0?'Loan Repayment':$line->transaction_description}}</td>
						<td>{{$line->transaction_period}}</td>
						<td>{{number_format($line->debit_amount)}}</td>
						<td>{{number_format($line->credit_amount)}}</td>
						<td>{{number_format($runningTotal +=$line->debit_amount-$line->credit_amount)}}</td>
					</tr>
				         
				      @endforeach
				      @endif
				<tr><td colspan="3"></td><th  colspan="3" class="bg-secondary">Running Balance:</th><th  class="bg-secondary">{{number_format($runningTotal)}} </th></tr>
				</tbody>
			</table>
			<div style="page-break-after: always;"></div>
			@endforeach
			@endif

		