<style type="text/css">
  table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
  }
  th, td {
    padding: 10px;
    text-align: left;
}
</style>
<table class="table">
  <thead>
    <th>SNo.</th>
    <th>Name</th>
    <th>ID No.</th>
    <th>Admission No.</th>
    <th>University</th>
    <th>Product</th>
    <th>Academic Yr.</th>
    <th>Principal</th>
    <th>Amount Paid</th>
    <th>Running Balance</th>
  </thead>
  <tbody>
    @if(count($results))
    @foreach($results as $key => $loan)
    <tr>
      <td>{{$loan['loan_serial_num']}}</td>
      <td>{{$loan['username']}}</td>
      <td>{{$loan['id_no']}}</td>
      <td>{{$loan['student_reg_num']}}</td>
      <td>{{$loan['university']}}</td>
      <td>{{$loan['loan_category_code']}}</td>
      <td>{{$loan['academic_year']}}</td>
      <td>{{number_format($loan['principal_disbursed'])}}</td>
      <td>{{number_format($loan['principal_repaid'])}}</td>
      <td>{{number_format($loan['outstanding_balance'])}}</td>
    </tr>
    
    @endforeach
    @endif
  </tbody>
</table>
