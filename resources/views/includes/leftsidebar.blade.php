<div class="sidenav">
    <div class="menu-title">
        <div class="menu-title-text">Loan Book</div>
    </div>
    <a href="{{route('home')}}"><i class="icon dripicons-home"></i>&nbsp; &nbsp;Dashboard</a>
    <button class="dropdown-btn"><i class="icon dripicons-briefcase"></i>&nbsp;&nbsp; Loans <i class="icon dripicons-chevron-down" style="margin-left:60%"></i>
    </button>
    <div class="dropdown-container">

        <a href="{{route('loans')}}"><i class="icon dripicons-briefcase"></i>&nbsp; &nbsp; View loans</a>
        <a href="{{route('addLoan')}}"><i class="icon dripicons-briefcase"></i>&nbsp; &nbsp;New loan</a>
        <a href="{{route('performing-loans')}}"><i class="icon dripicons-list"></i>&nbsp; &nbsp;Performing Loans</a>
        <a href="{{route('cleared-loans')}}"><i class="icon dripicons-list"></i>&nbsp; &nbsp;Cleared Loans</a>
        <a href="{{route('default-loans')}}"><i class="icon dripicons-list"></i>&nbsp; &nbsp;Default Loans</a>
        <a href="{{route('dormant-loans')}}"><i class="icon dripicons-list"></i>&nbsp; &nbsp;Dormant Loans</a>
    </div>
   <button class="dropdown-btn"><i class="icon dripicons-home"></i>&nbsp; &nbsp; Loan Book  <i class="icon dripicons-chevron-down" style="margin-left:42%"></i>

    </button>
    <div class="dropdown-container">
        <a href="{{route('loans')}}"><i class="icon dripicons-home"></i>&nbsp;&nbsp;Up-to-date</a>
        <a href="{{route('loansperyear')}}"><i class="icon dripicons-home"></i>&nbsp;&nbsp;Annually</a> 
        <a href="{{route('loansperquarter')}}"><i class="icon dripicons-home"></i>&nbsp;&nbsp;Quarterly</a>
    </div>
    <button class="dropdown-btn"><i class="icon dripicons-home"></i>&nbsp; &nbsp; Loan Book Reports  <i class="icon dripicons-chevron-down" style="margin-left:20%"></i>

    </button>
    <div class="dropdown-container">
        <a href="{{route('ageing')}}"><i class="icon dripicons-home"></i>&nbsp;&nbsp;Loan Ageing</a>
        <a href="#"><i class="icon dripicons-home"></i>&nbsp;&nbsp;Link 2</a>
    </div>
    <div class="menu-title">
        <div class="menu-title-text">Reports</div>
    </div>
     <a href="{{route('statements')}}"><i class="icon dripicons-graph-line"></i> &nbsp; &nbsp;Status</a>
    <a href="{{route('statement')}}"><i class="icon dripicons-home"></i>&nbsp;&nbsp; &nbsp;Statement</a>
    <button class="dropdown-btn"><i class="icon dripicons-home"></i>&nbsp; &nbsp; Collections  <i class="icon dripicons-chevron-down" style="margin-left:45%"></i>
    </button>
    <div class="dropdown-container">
        <a href="{{route('collections-report')}}"><i class="icon dripicons-briefcase"></i>&nbsp; &nbsp;Collection report</a>
    </div>
    <button class="dropdown-btn"><i class="icon dripicons-home"></i>&nbsp; &nbsp; Billing  <i class="icon dripicons-chevron-down" style="margin-left:60%"></i>
    </button>
    <div class="dropdown-container">
        <a href="{{route('billing-report')}}"><i class="icon dripicons-briefcase"></i>&nbsp; &nbsp;Bill Performance</a>
    </div>

 <button class="dropdown-btn"><i class="icon dripicons-home"></i>&nbsp; &nbsp; Clearance  <i class="icon dripicons-chevron-down" style="margin-left:48%"></i>
    </button>
    <div class="dropdown-container">
        <a href="{{route('clearanceprojection')}}"><i class="icon dripicons-briefcase"></i>&nbsp; &nbsp;Projection</a>
    </div>

</div>
