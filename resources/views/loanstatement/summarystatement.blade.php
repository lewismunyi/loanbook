
<div class="card">
            <div class="card-header"><h4>Loanee Statement</h4> </div>
<div class="card-body">




				<p> <a href="{{route('p_s_s',[$accountnum])}}" target="_blank" class="btn btn-info"><i class="icon dripicons-download"></i> Print statement</a></p>
				@if(count($headers))
					@foreach($headers  as $key => $header)
			<table class="table table-hover" id="tblStatement" border="1">
				<thead>
					<th colspan="4">Name:{{$header->names}} <br>ID Number:{{$header->idnumber}} <br>
							Accountnum:{{$header->accountnum}}
					</th>
				   
					<th  colspan="5">Product:{{$header->PRODUCTDESCRIPTION}} <br>Serial No:{{$header->loanserialno}} <th>
					
					
				</thead>
				<tr class="bg-light">				
				    <td>Loan Principal</td>
				    <td>Outstanding Principal</td>
				 	<td>Outstanding insurance</td>
					<td>Outstanding interest</td>
					<td>Outstanding ledgerfee</td>
					<td>Outstanding penalty</td>
				    <td colspan="3">Running Balance </td>
				
				</tr>
				<tr class="bg-secondary">				
				    <th>{{$header->loanprincipal}}</th>
				    <th>{{$header->outstanding_principal}}</th>
				 	<th>{{$header->outstanding_insurance}}</th>
					<th>{{$header->outstanding_interest}}</th>
					<th>{{$header->outstanding_ledgerfee}}</th>
					<th>{{$header->outstanding_penalty}}</th>
				    <th  colspan="3">{{$header->running_balance}} </th>
				
				</tr>
				<tr><td colspan="9"><center>Account Transactions</center></td></tr>
			<thead>
					<th>Transaction Ref.</th>
				    <th>Transaction Date</th>
				 	<th>Pay Mode</th>
				 	<th>Paid By</th>
				 	<th>Description</th>
					<th>Period</th>
					<th>Charges</th>
					<th>Payments</th>
					<th>Running Balance</th>
					
				</thead>
				<tbody>
					
					@if ($header->statementLinesGrouped->count())
					      @foreach ($header->statementLinesGrouped as $keyy => $line) 
					       
					      
				   <tr>
						<td>{{$line->documentnum}}</td>						
						<td>{{$line->transaction_date}}</td>
						<td>{{$line->paymode=='STATBANK'?'Employer':$line->paymode}}</td>
						<td>{{$line->originator=='0'?'':$line->originator}}</td>
						
						<td>{{$line->credit_amount>0?'Loan Repayment':$line->transaction_description}}</td>
						<td>{{ucwords(strtolower($line->transaction_period))}}</td>
						<td>{{number_format($line->debit_amount)}}</td>
						<td>{{number_format($line->credit_amount)}}</td>
						<td>{{number_format($runningTotal +=$line->debit_amount-$line->credit_amount)}}</td>
					</tr>
				         
				      @endforeach
				      @endif
				<tr><td colspan="7"></td><td class="bg-secondary">Running Balance:</td><td  class="bg-secondary">{{number_format($runningTotal)}} </td>
				</tr>
				</tbody>
			</table>
			@endforeach
			@endif
</div>
</div>	
		