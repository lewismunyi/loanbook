<style type="text/css">
  table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
  }
  th, td {
    padding: 10px;
    text-align: left;
}
</style>
<table class="table table-bordered">
    <tr>
      <th>Date</th>
      <th>Ref N.o</th>
      <th>Description</th>
      <th>Period</th>
      <th>Debit</th>
      <th>Credit</th>
      <th>Running Amount</th>
    </tr>
    <tr>
      <td>02-18-2017</td>
      <td>REFMCDSR67</td>
      <td>this is a description</td>
      <td>Feb 2017</td>
      <td>2200.02</td>
      <td>0</td>
      <td>230674.70</td>
    </tr>
    <tr>
      <td>02-18-2017</td>
      <td>REFMCDSR67</td>
      <td>this is a description</td>
      <td>Feb 2017</td>
      <td>2200.02</td>
      <td>0</td>
      <td>230674.70</td>
    </tr>
</table>
