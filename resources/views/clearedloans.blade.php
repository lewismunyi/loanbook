@extends('layouts.app')
@section('nav-left')
	<a class="navbar-brand" href="#"><h3>Cleared Loans</h3></a>
@endsection
@section('nav-search')
	<!-- <form onsubmit="return false;" class="statements-search form-inline my-2 my-lg-0" id="searchForm" v-on:submit="getLoans">
		<input class="form-control mr-sm-2" type="search" placeholder="Enter ID number" aria-label="Search" v-model="searchTerm">
		<button class="btn btn-outline-light my-2 mr-5" type="submit">Search</button>
	</form> -->
	{{--<form method="POST"  action="{{route('cleared-loans-filter')}}" enctype="multipart/form-data" class="statements-search form-inline my-2 my-lg-0" id="searchCollectionReport">--}}
	<form onsubmit="return false;" v-on:submit="loadDataFromAPI" class="statements-search form-inline my-2 my-lg-0" id="searchCollectionReport">
		{{ csrf_field() }}
        <div class="input-group input-daterange">          
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
                <input id="startDate1" placeholder="From Date" name="startDate" type="text" class="form-control" readonly="readonly" v-model="from">
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </span>
            <div class="text-white mt-auto"><span>&nbsp; to &nbsp;</span></div>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
                <input id="endDate1" placeholder="To Date" name="endDate" type="text" class="form-control" readonly="readonly" v-model="to">
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </span>
            &nbsp;&nbsp;
             <select class="custom-select mr-3" id="loanproduct" name="loanproduct" v-model="product">
				<option value="">Loan Product</option>
				<option >UG</option>
				<option >PI</option>
				<option >CI</option>
				<option >TVET</option>
				<option >AEF</option>
			 </select>
        </div>
       
        <button class="btn btn-outline-light my-2 ml-2 mr-5" type="submit">Get Loans</button>
    </form>
@endsection
@section('title', 'Loans')
@section('content')
<div class="row">
	<div class="col-lg-2 col-sm-12 offset-5">
		<a class="btn btn-success btn-block btn-export text-white" href="{{route('cleared-loans-csv')}}">Export CSV</a>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
				<table class="table">
					<thead>
						<th>SNo.</th>
						<th>Name</th>
						<th>ID No.</th>
						<th>Admission No.</th>
						<th>University</th>
						<th>Product</th>
						<th>Academic Yr.</th>
						<th>Principal</th>
						<th>Running Balance</th>
					</thead>
					<tbody>
						@if(count($loans))
						@foreach($loans as $key => $loan)
						<tr>
							<td>{{$loan->loan_serial_num}}</td>
							<td>{{$loan->username}}</td>
							<td>{{$loan->id_no}}</td>
							<td>{{$loan->student_reg_num}}</td>
							<td>{{$loan->university}}</td>
							<td>{{$loan->loan_category_code}}</td>
							<td>{{$loan->academic_year}}</td>
							<td>{{number_format($loan->principal_disbursed)}}</td>
							<td>{{number_format($loan->outstanding_balance)}}</td>
						</tr>
						@endforeach
                        {{--<tr v-for="loan in loans">--}}
                            {{--<td>@{{loan.data.loan_serial_num}}</td>--}}
                            {{--<td>@{{loan.data.username}}</td>--}}
                            {{--<td>@{{loan.data.id_no}}</td>--}}
                            {{--<td>@{{loan.data.student_reg_num}}</td>--}}
                            {{--<td>@{{loan.data.university}}</td>--}}
                            {{--<td>@{{loan.data.loan_category_code}}</td>--}}
                            {{--<td>@{{loan.data.academic_year}}</td>--}}
                            {{--<td>@{{loan.data.principal_disbursed}}</td>--}}
                            {{--<td>@{{loan.data.outstanding_balance}}</td>--}}
                        {{--</tr>--}}
						@endif
					</tbody>
				</table>
				{{ $loans->links() }}
			</div>
		</div>
	</div>
</div>
@endsection

@section('page-scripts')
<script type="text/javascript">
	$(function(){
		$('ul.pagination li a').click(function(e){
			//e.preventDefault();
		});
	});
		//Datepicker
	$(document).ready(function() {
		$('.input-daterange').datepicker({});
	});
	// Vue
    var clearedLoans = new Vue({
        el: '#body',
        data: {
            from: this.from,
			to: this.to,
			product: this.product,
            loans: [
                // {'serial': '1', 'name': 'x', 'id_no':' Too', 'admission': '2435', 'university': '88843935656', 'product': '20', 'academic_year': '3', 'principal':'3000', 'running_balance' : '50'}
            ]
        },
        mounted(){
            // this.loadDataFromAPI();
			// console.log("Hello mounted!");
            $("#startDate1").datepicker().on(
                "changeDate", () => {this.from = $('#startDate1').val()}
            );
            $("#endDate1").datepicker().on(
                "changeDate", () => {this.to = $('#endDate1').val()}
            );
        },
        methods: {
            loadDataFromAPI: function(){
                var self = this;
                console.log(this.product);
                console.log(this.from);
                console.log(this.to);
                ///get the data from the server/api
                axios.get('{{route('cleared-loans-filter')}}', {
                    params: {
                        'from': this.from,
                        'to': this.to,
                        'product': this.product
                    }
                })
				.then(function (response) {
					// handle success
                    self.loans = JSON.parse(response.data);
                    console.error("Self loans ni" + JSON.parse(self.loans));

				})
				.catch(function (error) {
					// handle error
					console.log("error" + error);
				})
				.then(function () {
					// always executed
					console.log("exiting...");
                    console.error("Self loans 2 ni" + self.loans);
				});
            }
        }
    });
</script>
@endsection
