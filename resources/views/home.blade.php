@extends('layouts.app')
@section('nav-left')
    <a class="navbar-brand" href="#"><h3>Dashboard</h3></a>
@endsection
@section('nav-search')
<form method="POST" id="frmRemittance" action="{{route('home')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="card">
                                    
                                    <select name="product" id="product" class="form-control">
                                        <option value="">Select product</option>
                                        @foreach($loanCategories as $loanCategory)
                                    
    <option value="{{$loanCategory->loan_category_code}}" {{($categorycode==$loanCategory->loan_category_code)? 'selected':''}}>{{$loanCategory->loan_category}}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <<div class="col-sm-3">
                                <div class="card">
                                <select name="academic_year" id="academic_year" class="form-control">
                                    <option value="">Select academic year</option>
                                    @foreach($academicYears as $academicYear)
                                      
    <option value="{{$academicYear->academic_year}}" {{($acadyr==$academicYear->academic_year)? 'selected':''}}>{{$academicYear->academic_year}}</option>
                                    @endforeach
                                </select>
                              </div>
                            </div>
                            <div class="form-group mt-auto">
                                <button class="btn btn-primary" id="btnValidate">Update Dashboard</button>
                            </div>
                        </div>
                    </form>
@endsection
@section('title', 'Loanbook')
@section('content')
<div class="container-fluid">
    <br>
    {{--Update dashboard content--}}
    <div class="card">
        <div class="card-body">
                    
            </div>
        </div>
    </div>
    <br>
    {{--Summaries--}}
    <section class="section">
        <div class="row">
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body widget">
                        <span class="widget-label">All Loans</span>
                        <h3>{{number_format($stats->all_count)}}</h3>
                        <h4>KSHs {{number_format($stats->all_amount)}}</h4>

                        <div>
                            




 @if($categorycode=='')
                           <a href="{{route('loans')}}" class="badge badge-success">Running : {{number_format($stats->running_count)}}</a>
                            <a href="{{route('cleared-loans')}}" class="badge badge-secondary">Cleared : {{number_format($stats->cleared_count)}}</a>
@else
<form method="POST" id="running" action="{{route('loansfiltered')}}" enctype="multipart/form-data">

                            {{ csrf_field() }}
                    <input name="product" id="product" type="hidden" value="{{$categorycode}}">
                    <button class="badge badge-success" id="btnunmatured">Running : {{number_format($stats->running_count)}}</button>
                             
    </form>


<form method="POST" id="running" action="{{route('cleared-loans')}}" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            <input name="product" id="product" type="hidden" value="{{$categorycode}}">
                            
                                    <button class="badge badge-secondary" id="btnunmatured">Cleared : {{number_format($stats->cleared_count)}}</button>
                             
    </form>
@endif
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body widget">
                        <span class="widget-label">Matured Loans</span>
                        <h3>{{number_format($stats->matured_count)}}</h3>
                        <h4>KSHs {{number_format($stats->matured_amount)}}</h4>

                        <div>
                            
                            <!-- <a href="" class="badge badge-danger">NPL : {{number_format($stats->npl_count)}}</a> -->


 @if($categorycode=='')
                           <a href="{{route('maturedfilter')}}" class="badge badge-success">Matured :  {{number_format($stats->matured_count)}}</a>
@else
<form method="POST" id="running" action="{{route('maturedfiltered')}}" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            <input name="product" id="product" type="hidden" value="{{$categorycode}}">
                            
                                    <button class="badge badge-success" id="btnunmatured">Un-Matured: {{number_format($stats->unmatured_count)}}</button>
                             
    </form>
@endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body widget">
                        <span class="widget-label">Un-Matured Loans</span>
                        <h3>{{number_format($stats->unmatured_count)}}</h3>
                        <h4>KShs {{number_format($stats->unmatured_amount)}}</h4>
                       

 @if($categorycode=='')
                             <a  href="{{route('unmaturedfilter')}}" class="badge badge-info"> Un-Matured: {{number_format($stats->unmatured_count)}}</a>
@else
    <form method="POST" id="running" action="{{route('unmaturedfilter')}}" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            <input name="product" id="product" type="hidden" value="{{$categorycode}}">
                            
                                    <button class="badge badge-info" id="btnunmatured">Un-Matured: {{number_format($stats->unmatured_count)}}</button>
                             
    </form>
@endif
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body widget">
                        <span class="widget-label">Non Performing</span>
                        <h3>{{number_format($stats->dormant_count)}}</h3>
                        <h4>{{number_format($stats->dormant_amount)}}</h4>

                        <div>
                            @if($categorycode=='')
                            <a href="{{route('dormant-loans')}}" class="badge badge-danger">Defaulters : {{number_format($stats->dormant_count)}}</a>
                            <!-- <a href="{{route('default-loans')}}" class="badge badge-danger">Dropouts : {{number_format($stats->defaulted_count)}}</a> -->
@else
    <form method="POST" id="running" action="{{route('dormantfilter')}}" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            <input name="product" id="product" type="hidden" value="{{$categorycode}}">
                            
                                    <button class="badge badge-danger" id="btndefaulters">Defaulters : {{number_format($stats->dormant_count)}}</button>
                             
    </form>
@endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

            <br>
    <section class="section">
        <div class="row">
            <div class="col-sm-7">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Chart Title</h4>
                    </div>
                    <div class="card-body">
                        <div id="loanGraph"></div>
                    </div>
                </div>
            </div>


            <div class="col-sm-5">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Chart Title</h4>
                    </div>
                    <div class="card-body">
                        <div id="loanDonut"></div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>


<!-- Modal -->

@endsection

@section('page-scripts')
<script type="text/javascript">
	$(function(){
		//tooltips
		$('[data-toggle="tooltip"]').tooltip();
		///////////Loans graph
		var chart = c3.generate({
			bindto: '#loanGraph',
			data: {
		        columns: [
		            ['Charged', 200, 100, 200,430, 150],
		            ['Paid',  20, 10, 40,50, 15]
		        ],
		        colors: { Charged: "#253449", Paid: "#ffc01b"},
		        type : 'bar'
		    },
		    axis: {
	            x: {
	                type: 'category', // this needed to load string x value
	                categories: [ 'Interest', 'Ledger Fee', 'Insurance','Principal', 'Penalty'],
	                //show: false
	            },
	            y: {
	                label: {
	                    text: '',
	                    position: 'outer-middle'
	                }
	            }
	        }
		});
		///////////////////////////////////


		///Loan stats
		var chart = c3.generate({
			bindto: '#loanDonut',
			data: {
		        columns: [
		            ['Performing', 74],
		            ['Non Performing', 26]
		        ],
		        //colors: { PL: "#253449", NPL: "#ffc01b"},
		        colors: { PL: "#253449", NPL: "#ffc01b"},
		        type : 'donut'
		    }
		});
		//////////////////////////////////////////

	});
</script>
@endsection
