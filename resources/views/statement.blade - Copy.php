@extends('layouts.app')
@section('title', 'Remittance')
@section('content')

<div class="page-content">
<div class="row">
<div class="col-xs-12">
    <h3>Loan Statement</h3>
</div>
</div>






<div class="row">

<div class="col-sm-4">
<div class="card">
	<div class="card-body">
		<p>Upload your remittance list</p>
		<form method="POST" id="frmRemittance" action="{{route('create-statement-lines')}}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="form-group">
				<label>National IDno.</label>
				<input type="text" name="idno" class="form-control" value="" required>
				
			</div>
			<div class="form-group">
				<label>Update balances</label>
				<input name="updatebalances" type="checkbox" value="1">
				
			</div>


			@if($errors->has('idno'))
          <span class="has-error help-block">{{ $errors->first('idno') }}</span>
      @endif

			<div class="form-group">
				<button class="btn btn-primary" id="btnValidate">Get Statement</button>
			</div>
		</form>



		@if($errors->all())
		@foreach($errors->all() as $error)
		<div class="alert alert-danger">
		    {{$error}}
		</div>
		@endforeach
		@endif


		<div id="remitOverlay" class="card-overlay">
				<h3 class="loading">Working...</h3>
		</div>
	</div>
</div>
</div>
@if(count($lines))
<div class="col-sm-8">
		<div class="card">
			<div class="card-header">
				<h4>Your remittance records</h4>
			</div>
			<div class="card-body">
				<p> <a href="{{route('p_d_s',[$accountnum])}}" target="_blank" class="btn btn-primary"><i class="icon dripicons-download"></i> Print statement</a></p>
			<table class="table" id="tblStatement">
				<thead>
					<th>Loan Serial</th>
				    <th>Transaction Date</th>
				 	<th>Description</th>
					<th>Period</th>
					<th>Charges</th>
					<th>Payments</th>
					<th>Running Balance</th>
					
				</thead>
				<tbody>
						
	
	{{$runningTotal = 0}}
					@if(count($lines))
					@foreach($lines as $line)
						<tr>
						<td>{{$line->serial_num}}</td>
						<td>{{$line->transaction_date}}</td>
						<td>{{$line->transaction_description}}</td>
						<td>{{$line->transaction_period}}</td>
						<td>{{$line->debit_amount}}</td>
						<td>{{$line->credit_amount}}</td>
						<td>{{$runningTotal +=$line->debit_amount-$line->credit_amount}}</td>
						
					</tr>
					@endforeach
					@endif
					
				</tbody>
			</table>
			</div>
		</div>
	</div>
	@endif
	
</div>



</div>

@endsection

@section('page-scripts')

<script>
    $(document).ready(function() {
        $('#tblStatement').DataTable();
    });



$(document).ready(function () {

    $("#myForm").validate({ // initialize the plugin
        // any other options,
        onkeyup: false,
        rules: {
            //...
        },
        messages: {
            //...
        }
    });

    $("#frmRemittance").ajaxForm({ // initialize the plugin
        // any other options,
        beforeSubmit: function () {
        	$('#remitOverlay').show();
            
        },
        success: function (response) {
        	//$('#remitOverlay').fadeOut();
        	if(response.status === false){
        		$('#remitOverlay').html('<h4>'+response.message+'</h4>')
        		var errs = response.errors;
        		console.log(errs);
        		for(i=0;i<errs.length;i++){
        			$('#remitOverlay').append('<p class="text-danger">'+errs[i]+'</p>');
        			$('#remitOverlay').append('<button class="btn btn-primary" onclick="hideOverlay()">OK</button>');
        		}
        	}else{
        		//$('#overlayContent').html(response.message);
        		window.location.href = response.url;
        	}
            console.log(response);
        }
    });

});

function hideOverlay(){
	$('#remitOverlay').html('<h3 class="loading">Working...</h3>').hide();
}

</script>

@endsection
