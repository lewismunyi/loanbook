@extends('layouts.app')
@section('nav-left')
	<a class="navbar-brand" href="#"><h3>Loans</h3></a>
@endsection
@section('nav-search')
	<form method="POST"  action="{{route('loans-filter')}}" enctype="multipart/form-data" class="statements-search form-inline my-2 my-lg-0" id="searchCollectionReport" autocomplete="off">
        {{ csrf_field() }}
		<div class="row">
			<div class="col-sm-3">
				Select the year 
			</div>
			<div class="col-sm-6">
				<select class="custom-select mr-3" id="loanproduct" name="loanproduct" required>
					<!-- <option value="">Select Period</option> -->
					<option  value="2017" >Financial year 18/19 1st Quarter</option>
					
				</select>
			</div>
		</div>
        <!-- <button class="btn btn-outline-light my-2 ml-2 mr-5" type="submit">Get Loans</button> -->
    </form>
@endsection

@section('title', 'Loans')
@section('content')
@if(count($loans))

	<div class="row mt-3 mb-3">

		<div class="col col-sm-12 col-lg-2 offset-5 float-right">
			<!-- Example split danger button -->
			<div class="btn-group pull-right">
				<button type="button" class="btn btn-success btn-lg">Download </button>
				<button type="button" class="btn btn-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="sr-only">Toggle Dropdown</span>
				</button>
				<div class="dropdown-menu">
					<a href="{{route('loansperquarter-csv')}}" class="btn btn-success btn-block btn-export dropdown-item">Download to CSV</a>
					<div class="dropdown-divider"></div>
					<a href="#" target="_blank" class="btn btn-success btn-block btn-export dropdown-item">Export PDF</a>
				</div>
			</div>
		</div>
	</div>
@endif
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
				<table class="table">
					<thead>
						<th>SNo.</th>
						<th>Name</th>
						<th>ID No.</th>
						<th>Admission No.</th>
						<th>University</th>
						<th>Product</th>
						<th>Academic Yr.</th>
						<th>Principal</th>
						
					</thead>
					<tbody>
						@if(count($loans))
						@foreach($loans as $key => $loan)
						<tr>
							<td>{{$loan->loan_serial_num}}</td>
							<td>{{$loan->username}}</td>
							<td>{{$loan->id_no}}</td>
							<td>{{$loan->student_reg_num}}</td>
							<td>{{$loan->university}}</td>
							<td>{{$loan->loan_category_code}}</td>
							<td>{{$loan->academic_year}}</td>
							<td>{{number_format($loan->principal_disbursed)}}</td>
							


						</tr>
						@endforeach
						@else
						<tr><td>No records founds</td></tr>
						@endif
					</tbody>
				</table>

				<nav>
					<ul class="pagination justify-content-center">
						{{$loans->links('vendor.pagination.bootstrap-4')}}
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>







<!-- Modal -->

@endsection

@section('page-scripts')
@endsection
