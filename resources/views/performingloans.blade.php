@extends('layouts.app')
@section('nav-left')
	<a class="navbar-brand" href="#"><h3>Performing Loans</h3></a>
@endsection
@section('nav-search')
	<!-- <form onsubmit="return false;" class="statements-search form-inline my-2 my-lg-0" id="searchForm" v-on:submit="getLoans">
		<input class="form-control mr-sm-2" type="search" placeholder="Enter ID number" aria-label="Search" v-model="searchTerm">
		<button class="btn btn-outline-light my-2 mr-5" type="submit">Search</button>
	</form> -->
	<form method="POST"  action="{{route('performing-loans-filter')}}" enctype="multipart/form-data" class="statements-search form-inline my-2 my-lg-0" id="searchCollectionReport" autocomplete="off">
        {{ csrf_field() }}
		<div class="row">
			<div class="col-sm-6">
				<input type="text" class="form-control" name="datefilter" value="" placeholder="Select date range" id="daterange" required/>
			</div>
			<div class="col-sm-6">
				<select class="custom-select mr-3" id="loanproduct" name="loanproduct" required>
					<option value="">Loan Product</option>
					<option >UG</option>
					<option >PI</option>
					<option >CI</option>
					<option >TVET</option>
					<option >AEF</option>
				</select>
			</div>
		</div>
        <button class="btn btn-outline-light my-2 ml-2 mr-5" type="submit">Get Loans</button>
    </form>
@endsection

@section('title', 'Loans')
@section('content')
	<div class="row">
		<div class="col-lg-2 col-sm-12 offset-5">
			<a class="btn btn-success btn-block btn-export text-white" href="{{route('performing-loans-csv')}}">Export CSV</a>
		</div>
	</div>

<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
				<table class="table">
					<thead>
						<th>SNo.</th>
						<th>Name</th>
						<th>ID No.</th>
						<th>Admission No.</th>
						<th>University</th>
						<th>Category</th>
						<th>Academic Yr.</th>
						<th>Amount</th>
						<th>Princ. Paid</th>
						<th>Int. Charged</th>
						<th>Int. Paid</th>
						<th>LF. Charged</th>
				<th>Last Bill Date</th>
				<th>Last Billed Employer</th>
					</thead>
					<tbody>
						@if(count($loans))
						@foreach($loans as $key => $loan)
						<tr>
							<td>{{$loan->loan_serial_num}}</td>
							<td>{{$loan->username}}</td>
							<td>{{$loan->id_no}}</td>
							<td>{{$loan->student_reg_num}}</td>
							<td>{{$loan->university}}</td>
							<td>{{$loan->loan_category_code}}</td>
							<td>{{$loan->academic_year}}</td>

							<td></td>
							<td></td>
						</tr>
						@endforeach
						@endif
					</tbody>
				</table>

				<nav>
					<ul class="pagination justify-content-center">
						{{$loans->links('vendor.pagination.bootstrap-4')}}
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>







<!-- Modal -->

@endsection

@section('page-scripts')
@endsection
