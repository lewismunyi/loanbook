
<div class="card">
            <div class="card-header"><h4>Bills Not Honored</h4> </div>
<div class="card-body">



				<p> <a href="{{route('p_d_s',[$randnum])}}" target="_blank" class="btn btn-info"><i class="icon dripicons-download"></i> Export to CSV</a></p>
				@if(count($billingheaders))
					@foreach($billingheaders  as $key => $billingheader)
@if ($billingheader->billingLines->where('bill_honored', 0)->count())
				<table width="60%">
				<thead>
					<th>Batch No</th>
					<th>employer</th>
					<th>billdate</th>					
					<th>officer</th>
				</thead>
					<tr>
					<td>{{$billingheader->batchno}}</td>
					<td>{{$billingheader->empcode}}</td>
					<td>{{$billingheader->datecreated }}</td>
					<td>{{$billingheader->officer }}</td>
					</tr>
					</table>
			<table class="table table-hover" id="tblStatement" border="1">
			
			<thead>
					<th>Name</th>
				    <th>idno</th>
				 	 <th>Serial No</th>
				 	<th>Last Pay Date</th>
					<th>Bill honored</th>
					
					
				</thead>
				<tbody>
					
					
					   @foreach ($billingheader->billingLines->where('bill_honored', 0)->chunk(10) as $chunk)
					     @foreach ($chunk as $line)	     
					      
				   <tr>
						<td>{{$line->name}}</td>						
						<td>{{$line->idno}}</td>
						<td>{{$line->loanserialno}}</td>
						<td>{{$line->lastpaydateafterbill}}</td>
						<td>No</td>
						
					</tr>
				         @endforeach 
				      @endforeach
				     
				
				</tbody>
				
			</table>
			 @endif
			@endforeach
			@endif
</div>
</div>	
		