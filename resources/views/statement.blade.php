@extends('layouts.app')
@section('nav-left')
	<a class="navbar-brand" href="#"><h3>Loan Statement</h3></a>
@endsection
@section('nav-search')
	<form class="statements-search form-inline my-2 my-lg-0" id="searchForm" method="POST" id="frmRemittance" action="{{route('create-statement-lines')}}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-sm-6">
				<input class="form-control mr-sm-2" type="search" placeholder="Enter ID number" aria-label="Search" v-model="searchTerm" name="idno" value="{{$idno}}">
			</div>
			<div class="col-sm-6">
				<label class="radio-label-vertical">
					
				<div class="custom-control custom-checkbox custom-control-inline">
					<input class="form-control mr-sm-2 custom-control-input mb-3" name="updatebalances" id="updateBalances" type="checkbox" value="1">
					<label class="text-white custom-control-label" for="updateBalances">Update balances</label>
				</div>
			</div>
		</div>
		<button class="btn btn-outline-light my-2 mr-5" id="btnValidate" type="submit">Get Statement</button>
	</form>
@endsection

@section('title', 'Loan statement')
@section('content')

<div class="page-content">

<div class="row">
	<div></div>
	<div class="col-sm-12">

		
@if($status=='notfound')
			 <div class="alert alert-danger alert-dismissible fade show">
			    <button type="button" class="close" data-dismiss="alert">&times;</button>
			    <strong>Record not Found!</strong>  ID Number {{$idno}} Has not benefited from any HELB loan product!! 
			  </div>
			  @endif
	</div>
@if(count($headers))

<div class="col-sm-12">

		<div class="card">
			<div class="card-header">
				<h4>Please select the statement you wish to view</h4>
			</div>
			<div class="card-body">


			
				  <div class="btn-group">
				 	 <button  type="button"   class="btn btn-primary" data-toggle="collapse" data-target="#detailedstatement">Provisional Statement</button>
				 	<button type="button" class="btn btn-primary"></button>
				    <button  type="button"   class="btn btn-info" data-toggle="collapse" data-target="#loaneestatement">Loanee Statement</button>
				</div> 
				@if($status=='nonperforming')
			 <div class="alert alert-danger alert-dismissible fade show">
			    <button type="button" class="close" data-dismiss="alert">&times;</button>
			    <strong>Defaulter!</strong> The account is in default. Last repayment date
			  </div>
			   @endif

			
			  @if($status=='cleared')
   <div class="alert alert-success alert-dismissible fade show">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Cleared!</strong> The account is cleared. 
  </div>
  @endif
		 @if($status=='performing')
   <div class="alert alert-info alert-dismissible fade show">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Performing!</strong> The account is in good standing
  </div>
  @endif
			<div id="detailedstatement" class="collapse">
				@include('loanstatement.detailedstatement') 
			</div>
			<div id="loaneestatement" class="collapse">
				@include('loanstatement.summarystatement')
			</div>
		</div>
		</div>
	</div>
	@endif
	
</div>
</div>

@endsection

@section('page-scripts')

<script>
    $(document).ready(function() {
        $('#tblStatement').DataTable();
    });



$(document).ready(function () {

    $("#myForm").validate({ // initialize the plugin
        // any other options,
        onkeyup: false,
        rules: {
            //...
        },
        messages: {
            //...
        }
    });

    $("#frmRemittance").ajaxForm({ // initialize the plugin
        // any other options,
        beforeSubmit: function () {
        	$('#remitOverlay').show();
            
        },
        success: function (response) {
        	//$('#remitOverlay').fadeOut();
        	if(response.status === false){
        		$('#remitOverlay').html('<h4>'+response.message+'</h4>')
        		var errs = response.errors;
        		console.log(errs);
        		for(i=0;i<errs.length;i++){
        			$('#remitOverlay').append('<p class="text-danger">'+errs[i]+'</p>');
        			$('#remitOverlay').append('<button class="btn btn-primary" onclick="hideOverlay()">OK</button>');
        		}
        	}else{
        		//$('#overlayContent').html(response.message);
        		window.location.href = response.url;
        	}
            console.log(response);
        }
    });

});

function hideOverlay(){
	$('#remitOverlay').html('<h3 class="loading">Working...</h3>').hide();
}

</script>

@endsection
