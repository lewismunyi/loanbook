@extends('layouts.app')
@section('nav-left')
    <a class="navbar-brand" href="#"><h3>Add new loan</h3></a>
@endsection
<style>
    .upload-btn-wrapper {
        position: relative;
        overflow: hidden;
        display: inline-block;
    }
    .upload-btn-wrapper input[type=file] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }
</style>
@section('nav-search')
    <form action = "{{route('upload')}}" method="POST" class="statements-search form-inline my-2 my-lg-0 upload-btn-wrapper" id="searchForm">
        {{csrf_field()}}
        <input class="form-control mr-sm-2" type="file" id="uploadfield" name="uploadfield" placeholder="Upload valid CSV file" aria-label="Search" v-model="searchTerm">
        <a class="btn btn-outline-light my-2 mr-5 text-white" >Upload CSV</a>
        <button type="submit" name="uploadcsv" id="uploadcsv" style="display: none;">Upload CSV</button>
    </form>
@endsection
@section('title', 'Loans')
@section('content')
    <br>
    <div class="container">
        <div class="row">
            <div class="col col-sm-12">
                <div class="d-flex justify-content-center" id="notify" role="alert">
                    <strong id="notify-text"></strong>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{route('addLoan')}}">
                    {{csrf_field()}}
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Personal details</legend>
                        <div class="row d-flex justify-content-center">
                            <div class="col col-sm-12 col-lg-2">
                                <div class="form-group">
                                    <label for="id_no">ID. Number</label>
                                    <input type="number" class="form-control" id="id_no" name="id_no" placeholder="123456" required>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-lg-6">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="John Doe" required>
                                </div>
                            </div>
                            <div class="col col-sm-12 col-lg-4 ">
                                <div class="form-group">
                                    <label for="reg_no">Registration Number</label>
                                    <input type="text" class="form-control" id="reg_no" name="reg_no" placeholder="P15/36076/2015" required>
                                </div>
                            </div>

                        </div>
                        <div class="row mt-3">
                            <div class="col col-sm-12 col-lg-4">
                                <div class="form-group">
                                    <label for="institution">Institution</label>
                                    <select class="form-control" id="institution" name="institution" required>
                                        <option selected >Select one</option>
                                        @foreach($institutions as $institution)
                                            <option value="{{$institution->institutionname}}">{{$institution->institutionname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col col-sm-12 col-lg-4 ">
                                <div class="form-group">
                                    <label for="academic_year">Academic year</label>
                                    <select class="form-control" id="academic_year" name="academic_year" required>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col col-sm-12 col-lg-4">
                                <div class="form-group">
                                    <label for="graduation">Graduation year</label>
                                    <select class="form-control" id="graduation" name="year_of_completion" required>
                                        <option value="" selected>Choose graduation value</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Loan details</legend>
                        <div class="row d-flex justify-content-center">
                            <div class="col col-sm-12 col-lg-4 ">
                                <div class="form-group">
                                    <label for="serial_no">Serial Number</label>
                                    <input type="number" class="form-control" id="serial_no" name="serial_no" placeholder="123456" required>
                                </div>
                            </div>
                            <div class="col col-sm-12 col-lg-4 ">
                                <div class="form-group">
                                    <label for="principle">Principle Loan</label>
                                    <input type="number" class="form-control" id="principle" name="principle_loan" placeholder="30000" required>
                                </div>
                            </div>
                            <div class="col col-sm-12 col-lg-4">
                                <div class="form-group">
                                    <label for="loanproduct">Loan Product</label>
                                    <select class="form-control" id="loanproduct" name="loanproduct" required>
                                        <option selected >Select one</option>
                                        @foreach($loanproducttypes as $loanproducttype)
                                        <option value="{{$loanproducttype->productdescription}}">{{$loanproducttype->productdescription}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
            <div class="row d-flex justify-content-center mt-3">
                    <button class="btn btn-dark btn-success" value="submit" type="submit">Submit</button>
            </div>
                </form>
            </div>
        </div>
            </div>
@endsection
@section('page-scripts')
    <script src="{{asset('js/papaparse.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('ul.pagination li a').click(function(e){
                //e.preventDefault();
            });
        });
        function showNotification(text, animation, type){
            if (type == 'danger'){
                $('#notify-text').text(text);
                $('#notify').addClass('alert alert-danger animated ' + animation);
                //wait for animation to finish before removing classes
                window.setTimeout( function(){
                    $('#notify-text').empty();
                    $('#notify').removeClass('alert alert-danger animated ' + animation);
                }, 5000);
            }
            else if (type == 'success') {
                $('#notify-text').text(text);
                $('#notify').addClass('alert alert-success animated ' + animation);
                //wait for animation to finish before removing classes
                window.setTimeout( function(){
                    $('#notify-text').empty();
                    $('#notify').removeClass('alert alert-success animated ' + animation);
                }, 5000);
            }
            else if (type == 'info'){
                $('#notify-text').text(text);
                $('#notify').addClass('alert alert-dark animated ' + animation);
                //wait for animation to finish before removing classes
                window.setTimeout( function(){
                    $('#notify-text').empty();
                    $('#notify').removeClass('alert alert-dark animated ' + animation);
                }, 5000);
            }
            else if(type == 'warning') {
                $('#notify-text').text(text);
                $('#notify').addClass('alert alert-warning animated ' + animation);
                //wait for animation to finish before removing classes
                window.setTimeout( function(){
                    $('#notify-text').empty();
                    $('#notify').removeClass('alert alert-warning animated ' + animation);
                }, 5000);
            }
        };

        var i;
        for (i = (new Date()).getFullYear(); i <= (new Date()).getFullYear()+30; i++){
            var option = document.createElement("option");
            option.value = i;
            option.text = i;
            document.getElementById("graduation").appendChild(option);
        }

        var inputfield = document.getElementById('uploadfield');
        inputfield.addEventListener('change', ()=> {
            var file = inputfield.files[0];
            if (file.name.slice(-4) !== ".csv" || file.type !== "application/vnd.ms-excel"){
                showNotification("Upload a valid .csv or .xlxs file", "shake", 'danger');
            }
            else {
                Papa.parse(file, {
                    complete: function(results) {
                        headers = results.data[0];
                        if (headers.length == 15 && headers[0] == "S/NO" && headers[1] == "NAME"  && headers[2] == "ADM NO."  && headers[3] == "INSTITUTION"  && headers[4] == "WARD"  && headers[5] == "ID. NO"  && headers[6] == "APPLICANT CELLPHONE"  && headers[7] == "POSTAL ADDRESS"  && headers[8] == "PARENT/GUARDIAN NAME"  && headers[9] == "PARENT OR GUARDIAN CELLPHONE"  && headers[10] == "MATURITY YEAR"  && headers[11] == "GUARANTOR NAME"  && headers[12] == "GUARANTOR CELLPHONE"  && headers[13] == "AMOUNT AWARDED"  && headers[14] == "FAINANCIAL YEAR"  ){
                            showNotification("Valid document. Uploading data...", "fadeIn", "success");
                            // console.log("Success! " + headers[0]);
                            $("#uploadcsv").click();
                        }
                        else {
                            showNotification("Document does not have matching column names. This may cause errors further ahead. Please choose a correct file", "shake", 'warning');
                        }
                    }
                });
            }

        });

    </script>
@endsection